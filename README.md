This repo was created on 08/11/2022.

It was created from an export of the AGSi_Documents repo and is intended to be the repo for the standard part of the AGSi project.

The original repo will be retained for the guidance documentation only.

The purpose of this is to separate the management of the guidance and standard.

More to follow.


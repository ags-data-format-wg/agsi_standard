


# AGSi Standard / 4. Model

## 4.6. agsiModelAlignment

### 4.6.1. Object description

An alignment comprises the geometry and metadata defining a line of interest, most commonly used for the line of a section (cross section, fence or profile  - see <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_Model_Sections#terminology-section-or-fence-or-profile-2d-or-3d">Guidance for discussion of terminology</a>). Alignments are typically included in a primary 3D model (alternatively a 2D map) but the sections themselves are normally defined as separate models because each 2D section will have its own coordinate system. The *[alignmentID](./Standard_Model_agsiModelAlignment.md#alignmentid)* attribute can be referenced by section models to provide a link to the alignment defined here. Sections must be drawn in the vertical (Z) plane, with alignments defined in the XY plane. The geometry is specified using an embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object referencing an external file.

The parent object of [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) contains the following embedded child objects: 

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)

[agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) has associations (reference links) with the following objects: 

- [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) has the following attributes:


- [alignmentID](#alignmentid)
- [alignmentName](#alignmentname)
- [description](#description)
- [agsiGeometry](#agsigeometry)
- [startChainage](#startchainage)
- [remarks](#remarks)


### 4.6.2. Attributes

##### alignmentID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for the alignment. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. May be referenced by *[alignmentID](./Standard_Model_agsiModel.md#alignmentid)* attribute of (a different) [agsiModel](./Standard_Model_agsiModel.md) object. [Identifiers](./Standard_General_Rules.md#1510-identifiers) for alignmentID shall be unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Condition:* Recommended  
*Example:* ``sectionAA``

##### alignmentName
Name or short description of what this alignment represents.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Section AA``

##### description
More verbose description, as required.  
*Type:* string  
*Example:* ``East-west section through site``

##### agsiGeometry
An embedded [Geometry group](./Standard_Geometry_Intro.md) object defining the geometry of the  alignment as a 2D line in XY space. This will be an embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object pointing to a line defined in an external file.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  


##### startChainage
The section chainage/baseline distance for the first point defined on the alignment. Assumed to be zero if not used.  
*Type:* number  
*Example:* ``1000``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``


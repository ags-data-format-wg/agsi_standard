


# AGSi Standard / 4. Model

## 4.5. agsiModelBoundary

### 4.5.1. Object description

An [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object defines the model boundary, i.e. the maximum extent of the model. Any elements or parts of elements lying outside the boundary are deemed to not be part of the model. Only one boundary per [agsiModel](./Standard_Model_agsiModel.md) is permitted. Only plan boundaries with vertical sides are permitted, defined by either limiting coordinates, or a bounding closed polygon. The top and base may be either a flat plane at a defined elevation, or a surface. Top boundary may not be required, depending on nature of model and/or software/application used (to be confirmed in specification). 

The parent object of [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) contains the following embedded child objects: 

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)

[agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) has the following attributes:


- [boundaryID](#boundaryid)
- [description](#description)
- [minX](#minx)
- [maxX](#maxx)
- [minY](#miny)
- [maxY](#maxy)
- [topElevation](#topelevation)
- [bottomElevation](#bottomelevation)
- [agsiGeometryBoundaryXY](#agsigeometryboundaryxy)
- [agsiGeometrySurfaceTop](#agsigeometrysurfacetop)
- [agsiGeometrySurfaceBottom](#agsigeometrysurfacebottom)
- [remarks](#remarks)


### 4.5.2. Attributes

##### boundaryID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for the model boundary. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it.  If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for boundaryID should be unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``BoundarySitewide``

##### description
Short description.  
*Type:* string  
*Example:* ``Boundary for Geological Model: sitewide``

##### minX
Minimum X for box boundary.  
*Type:* number  
*Example:* ``20000``

##### maxX
Maximum X for box boundary.  
*Type:* number  
*Example:* ``35000``

##### minY
Minimum Y for box boundary.  
*Type:* number  
*Example:* ``10000``

##### maxY
Maximum Y for box boundary.  
*Type:* number  
*Example:* ``15000``

##### topElevation
Elevation (Z) of top plane of model for box boundary.  
*Type:* number  
*Example:* ``40``

##### bottomElevation
Elevation (Z) of bottom plane of model for box boundary.  
*Type:* number  
*Example:* ``-40``

##### agsiGeometryBoundaryXY
Embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object that provides the geometry of the closed polygon defining the plan extent of model, as an alternative to the box boundary. Use with caution as this may not be supported by all software/applications. Confirm use in specification.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  


##### agsiGeometrySurfaceTop
Embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object that defines the top of the model, as an alternative to the box boundary. Use with caution as this may not be supported by all software/applications. May not be required for some software/applications. Confirm use in specification.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  


##### agsiGeometrySurfaceBottom
Embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object that defines the base of the model, as an alternative to the box boundary. Use with caution as this may not be supported by all software/applications. Confirm use in specification.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``





# AGSi Standard / 2. Root

## 2.3. agsFile

### 2.3.1. Object description

Metadata for the [AGSi package](./Standard_General_Definitions.md#agsi-package) (which comprises the [AGSi file](./Standard_General_Definitions.md#agsi-file) and ([included](./Standard_General_Definitions.md#agsi-included-files)) ([supporting files](./Standard_General_Definitions.md#agsi-supporting-files)) ).  The [AGSi package](./Standard_General_Definitions.md#agsi-package) should be treated as a document in accordance with standards established for the project. The attributes provided align with good practice BIM, in accordance with ISO19650. It is recommended that, where possible, this object is output at the top of the file, after the schema object, for human readability.

The parent object of [agsFile](./Standard_Root_agsFile.md) is [root](./Standard_Root_Intro.md)

[agsFile](./Standard_Root_agsFile.md) has the following attributes:


- [fileUUID](#fileuuid)
- [title](#title)
- [projectTitle](#projecttitle)
- [description](#description)
- [producedBy](#producedby)
- [fileURI](#fileuri)
- [reference](#reference)
- [revision](#revision)
- [date](#date)
- [status](#status)
- [statusCode](#statuscode)
- [madeBy](#madeby)
- [checkedBy](#checkedby)
- [approvedBy](#approvedby)
- [remarks](#remarks)


### 2.3.2. Attributes

##### fileUUID
Universal unique identifier ([UUID](./Standard_General_Rules.md#1511-uuids)) for the [AGSi package](./Standard_General_Definitions.md#agsi-package). This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.   
*Type:* string ([UUID](./Standard_General_Rules.md#1511-uuids))  
*Example:* ``98e17952-c99d-4d87-8f01-8ba75d29b6ad``

##### title
Title of the [AGSi package](./Standard_General_Definitions.md#agsi-package) (as used for document management system).  
*Type:* string  
*Condition:* Required  
*Example:* ``Stage 3 Sitewide Ground models``

##### projectTitle
Name of project (as used for document management system).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham City Metro Purple Line, C999 Geotechnical Package X``

##### description
Additional description, if required.  
*Type:* string  
*Example:* ``Geological model and geotechnical design models produced for Stage 4 design``

##### producedBy
Organisation that produced this [AGSi package](./Standard_General_Definitions.md#agsi-package).  
*Type:* string  
*Condition:* Required  
*Example:* ``ABC Consultants``

##### fileURI
[URI](./Standard_General_Definitions.md#uri) (link address) for the location of this [AGSi package](./Standard_General_Definitions.md#agsi-package) within the project document system. Spaces are not permitted in [URI](./Standard_General_Definitions.md#uri) (link address) strings. Refer to [ 1.6.6. URI](./Standard_General_Formats.md#166-uri) for how to handle spaces in file paths or names.  
*Type:* string ([uri](./Standard_General_Formats.md#166-uri))  
*Condition:* Recommended  
*Example:* ``https://gothammetro.sharepoint.com/C999/docs/C999-ABC-AX-XX-M3-CG-1234``

##### reference
Document reference (typically in accordance with ISO19650, BS1192 or project standards).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``C999-ABC-AX-XX-M3-CG-1234``

##### revision
Revision reference (typically in accordance with ISO19650 or BS1192 or project standards).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``P1``

##### date
Date of production.  
*Type:* string ([ISO date](./Standard_General_Formats.md#165-iso-date-andor-time))  
*Condition:* Recommended  
*Example:* ``2018-10-05``

##### status
Status, typically following recommendations of BS8574.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Final``

##### statusCode
Status code in accordance with ISO19650 (or BS1192 suitability code).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``S2``

##### madeBy
Person(s) identified as originator.  
*Type:* string  
*Example:* ``A Green``

##### checkedBy
Person(s) identified as checker.  
*Type:* string  
*Example:* ``P Brown``

##### approvedBy
Person(s) identified as approver.  
*Type:* string  
*Example:* ``T Black``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


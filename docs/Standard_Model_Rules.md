# AGSi Standard / 4. Model

## 4.2. Model rules and conventions

This section details rules and conventions applicable to the Model group of objects.
It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [1.5. General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.

### 4.2.1. Models

There may be more than one model defined in an
[AGSi file](./Standard_General_Definitions.md#agsi-file).

!!! Note
    For further information on what is considered to be a single model in an AGSi context,
    refer to <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_Model_General">Guidance on usage, Model - Overview</a>.

If there is only one model in the
[AGSi file](./Standard_General_Definitions.md#agsi-file), then the
[agsiModel](./Standard_Model_agsiModel.md)
object must be included within an array given that the parent attribute data type is array, in accordance with
[1.5.4. Arrays](./Standard_General_Rules.md#154-arrays).
Replacing the array with a single object is not permitted.

### 4.2.2. Model coordinate system

The coordinate system for each model should be defined using the
[*coordSystemID*](./Standard_Model_agsiModel.md#coordsystemid) attribute of the
[agsiModel](./Standard_Model_agsiModel.md) object, referencing the
[*systemID*](./Standard_Project_agsProjectCoordinateSystem.md#systemid) identifier for the relevant
[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md#systemID) object.

If the coordinate system(s) are not defined using the above method, then this information shall be conveyed in supporting documentation.

Further information on coordinate systems is provided in
[3.2.2. Coordinate systems](./Standard_Project_Rules.md#322-coordinate-systems).


### 4.2.3. Model elements

Model elements ([agsiModelElement](./Standard_Model_agsiModelElement.md) objects)
comprising volumetric units that cross or overlap are permitted in AGSi. Whether such a model is correct or appropriate is for the user to determine.

Each model element ([agsiModelElement](./Standard_Model_agsiModelElement.md)) should have geometry assigned to it by embedding a single object from the [Geometry group](./Standard_Geometry_Intro.md) using the
[*agsiGeometry*](./Standard_Model_agsiModelElement.md#agsigeometry) attribute. It is recommended that the target Geometry group object class is identified using the
[*geometryObject*](./Standard_Model_agsiModelElement.md#geometryobject) attribute of
[agsiModelElement](./Standard_Model_agsiModelElement.md) as some software/applications may require this.

### 4.2.4. Model element limiting area

The [*agsiGeometryAreaLimit*](./Standard_Model_agsiModelElement.md#agsigeometryarealimit)
attribute of [agsiModelElement](./Standard_Model_agsiModelElement.md)
allows a limiting plan area for the element to be defined by embedding an
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object.
If used, the geometry referenced must represent a closed polygon. The polygon acts as a vertical 'cookie cutter' clipping the element boundary, i.e. the model element is the part contained within the polygon only.

This feature should be used with caution as it may not be supported by all software/applications.
Project requirements for use of this attribute should be confirmed in the
[specification](./Standard_General_Specification.md).

### 4.2.5. Model boundary

An [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object defines the model boundary, i.e. the maximum extent of the model. Any model elements or parts of model elements lying outside the boundary are deemed to be not part of the model.

Only model boundaries with vertical sides are supported by this method.

Only one boundary object per [agsiModel](./Standard_Model_agsiModel.md) is permitted.

The plan (XY) extent of the model should be defined by ONE of the following methods:

- limiting coordinates (minX, maxX, minY and maxY), or
- closed polygon (embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)

The top and bottom (base) of the model should be defined by ONE of the following methods:

- elevation (Z value) of a plane, or
- surface (embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)

Definition of the top surface of the model, which is typically the ground surface (terrain), may not be required in some cases. This may be software/application dependent.

Use of this [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
object is optional if the model elements are already curtailed at the model boundary.

Some aspects of this feature may not be supported by all software/applications.
Project requirements for use of model boundaries, including whether a top boundary is required, should be described in the
[specification](./Standard_General_Specification.md).

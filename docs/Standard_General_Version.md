# AGSi Standard / 1. General

## Version info

Refer to the AGSi Gitlab 'Issue' referenced for full details and rationale of each change:

<a href="https://gitlab.com/ags-data-format-wg/agsi/AGSi_Standard/-/issues/?sort=created_date&state=all" target="_blank">https://gitlab.com/ags-data-format-wg/agsi/AGSi_Standard/-/issues</a>.

### Version 1.0.1

This version, published June 2024, includes clarifications to some rules, a few new codes, corrected/updated JSON Schema files and minor corrections/clarifications. Further details follow.

Clarifications to rules:

- 1.5.7. Required attributes (issue #181 )
- 1.6.2. Reference to object ID (issue #182)

Additions to Code list - Parameters (9.1.1), per issues #178, #104:

- AnalysisDrainageCondition
- AngleDilation
- PoissonsRatio
- CoefficientLateralEarthPressureAtRest 
- CoefficientLateralEarthPressureActive
- CoefficientLateralEarthPressurePassive
- CoefficientLateralEarthPressureStar
- Permeability
- PermeabilityHorizontal
- PermeabilityVertical

JSON Schema

- *agsSchema* and *agsFile* now correctly identified as required (issue #173)
- Correction to validation condition for *profileCoordinates* in *agsiObservationExpHole* (issue #176)
- Link referenced in $schema at head of file updated/corrected (issue #171)
- General update to reflect the schema documentation changes
- JSON schema using version 2020-12 of JSON Schema created and made available, in addition to existing 2019-09 version. They are effectively the same, bar a reference to the version at the top (issue #183)
- 8.3 JSON Schema page updated accordingly, including clarifying Note on format checking
- Example for *link* in *agsSchema* updated to point to latest file (issue #175)

Minor typos etc, per issue #174:

- Added a previously missing example for *fileFormatVersion* in *agsiDataPropertyFromFile*
- Amended examples for *documentSetID* in *agsProject DocumentSet* to "ExampleDocSetID". Also similarly changed the example of all other object attributes that reference documentSetID to match. This is to help the the AGSi template file (new - see Guidance) pass validation.
- Amended example for *remarks* in *agsProjectCodeSet*
- Examples that include links or references to schema updated to 1.0.1 and for new Gitlab folder structure (issue #177)
- Correction of a few other minor typos

### Version 1.0.0

=======
<a href="https://gitlab.com/ags-data-format-wg/AGSi_Standard/-/issues/?sort=created_date&state=all" target="_blank">https://gitlab.com/ags-data-format-wg/AGSi_Standard/-/issues</a>.

### Version 1.0.1

This version, published June 2024, includes clarifications to some rules, a few new codes, corrected/updated JSON Schema files and minor corrections/clarifcations. Further details follow.

Clarifications to rules:

- 1.5.7. Required attributes (issue #181 )
- 1.6.2. Reference to object ID (issue #182)

Additions to Code list - Parameters (9.1.1), per issues #178, #104:

- AnalysisDrainageCondition
- AngleDilation
- PoissonsRatio
- CoefficientLateralEarthPressureAtRest 
- CoefficientLateralEarthPressureActive
- CoefficientLateralEarthPressurePassive
- CoefficientLateralEarthPressureStar
- Permeability
- PermeabilityHorizontal
- PermeabilityVertical

JSON Schema

- *agsSchema* and *agsFile* now correctly identified as required (issue #173)
- Correction to validation condition for *profileCoordinates* in *agsiObservationExpHole* (issue #176)
- Link referenced in $schema at head of file updated/corrected (issue #171)
- General update to reflect the schema documentation changes
- JSON schema using version 2020-12 of JSON Schema created and made available, in addition to existing 2019-09 version. They are effectively the same, bar a reference to the version at the top (issue #183)
- 8.3 JSON Schema page updated accordingly, including clarifying Note on format checking
- Example for *link* in *agsSchema* updated to point to latest file (issue #175)

Minor typos etc, per issue #174:

- Added a previously missing example for *fileFormatVersion* in *agsiDataPropertyFromFile*
- Amended examples for *documentSetID* in *agsProject DocumentSet* to "ExampleDocSetID". Also similarly changed the example of all other object attributes that reference documentSetID to match. This is to help the the AGSi template file (new - see Guidance) pass validation.
- Amended example for *remarks* in *agsProjectCodeSet*
- Examples that include links or references to schema updated to 1.0.1 and for new Gitlab folder structure (issue #177)
- Correction of a few other minor typos

### Version 1.0.0

Version **1.0.0** of the standard and schema, published 30 November 2022, was the first formal release of AGSi.

### beta versions

[Click here for the version history of the beta versions that preceded v1.0.0](./Standard_General_VersionBeta.md)

!!! Note
    This includes links to the documentation for some of the beta versions.

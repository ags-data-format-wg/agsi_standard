


# AGSi Standard / 7. Data

## 7.6. agsiDataParameterValue

### 7.6.1. Object description

Each [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) object provides the data for a single defined parameter. The parameter value conveyed may be numeric, a profile of numeric values (e.g. a design line) or text. Refer to [7.2. Data rules and conventions](./Standard_Data_Rules.md) for further details.

The parent object of [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) is [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) has associations (reference links) with the following objects: 

- [agsProjectCode](./Standard_Project_agsProjectCode.md)

[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) has the following attributes:


- [dataID](#dataid)
- [codeID](#codeid)
- [caseID](#caseid)
- [valueNumeric](#valuenumeric)
- [valueText](#valuetext)
- [valueProfileIndVarCodeID](#valueprofileindvarcodeid)
- [valueProfile](#valueprofile)
- [remarks](#remarks)


### 7.6.2. Attributes

##### dataID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this data object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Data group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``42f18976-7352-4f67-9a6e-df89788343a7``

##### codeID
Code that identifies the parameter. Codes should be defined in either an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. The codes used by the instances of this object contained within a single parent object instance shall be unique, except that if caseID is used then only the combination of codeID and caseID needs to be unique.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Condition:* Required  
*Example:* ``UndrainedShearStrength``

##### caseID
Code (or text) that identifies the use case for a parameter. See  [7.2.4. Use of (data) case](./Standard_Data_Rules.md#724-use-of-data-case) for example use cases. If the input is a code, this shall be defined in an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. May be left blank or omitted, but the combination of codeID and caseID shall be unique for the instances of this object contained within a single parent object instance.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object, or text)  
*Example:* ``EC7Pile``

##### valueNumeric
Numeric value of parameter, if applicable.  
*Type:* number  
*Condition:* Recommended  
*Example:* ``75``

##### valueText
Text based value of parameter, if applicable. For a profile (see below), this could be used for a concise description or representation of the profile. Unless specified otherwise, this attribute should only be used when the value is not numeric, i.e. valueNumeric not used.  
*Type:* string  
*Example:* ``100 + 6z (z=0 @ +6.0mOD)``

##### valueProfileIndVarCodeID
Code that identifies the independent variable for a profile, i.e. what the property value varies against. The code shall be defined in an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Condition:* Recommended  
*Example:* ``Elevation``

##### valueProfile
The profile of values as an ordered list of tuples of [independent variable value, parameter value]. Typically used to represent design lines. Refer to [1.6.9. Profiles or arrays of coordinate tuples](./Standard_General_Formats.md#169-profiles-or-arrays-of-coordinate-tuples) for further information.  
*Type:* array ([profile](./Standard_General_Formats.md#169-profiles-or-arrays-of-coordinate-tuples))  
*Condition:* Recommended  
*Example:* ``[[6,100],[-24,280]]``

##### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``


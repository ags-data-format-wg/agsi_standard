


# AGSi Standard / 6. Geometry

## 6.6. agsiGeometryVolFromSurfaces

### 6.6.1. Object description

An [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object defines an element as the volumetric element (solid) between top and/or bottom surfaces. This is a linking object between model element and the source geometry for the surfaces, which will normally be [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects. Refer to [6.2.3. Volumes from surfaces](./Standard_Geometry_Rules.md#623-volumes-from-surfaces-agsigeometryvolfromsurfaces) for full details of how the volume should be interpreted.

The parent object of [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) is [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) contains the following embedded child objects: 

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md)

[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [agsiGeometryTop](#agsigeometrytop)
- [agsiGeometryBottom](#agsigeometrybottom)
- [remarks](#remarks)


### 6.6.2. Attributes

##### geometryID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this geometry object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Geometry group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``GeologyGCC``

##### description
Short description of geometry defined here.  
*Type:* string  
*Example:* ``Gotham Clay``

##### agsiGeometryTop
Geometry for top surface, as embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object. Definition of both top and bottom surfaces is recommended to minimise the risk of error. Refer to [6.2.3. Volumes from surfaces](./Standard_Geometry_Rules.md#623-volumes-from-surfaces-agsigeometryvolfromsurfaces) for further details.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object)  
*Condition:* Required if [*agsiGeometryBottom*](#agsigeometrybottom) not used  


##### agsiGeometryBottom
Geometry for bottom surface, as embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object. Definition of both top and bottom surfaces is recommended to minimise the risk of error. Refer to [6.2.3. Volumes from surfaces](./Standard_Geometry_Rules.md#623-volumes-from-surfaces-agsigeometryvolfromsurfaces) for further details.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object)  
*Condition:* Required if [*agsiGeometryTop*](#agsigeometrytop) not used  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


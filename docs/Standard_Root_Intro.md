# AGSi Standard / 2. Root

## 2.1. Root schema overview

### 2.1.1. Usage

The top level (ultimate parent) of the AGSi schema is called the **[root schema](./Standard_General_Definitions.md#root-schema-root-object)**.
This is the container for the remainder of the schema.

The root schema is a single object, known as the **[root object](./Standard_General_Definitions.md#root-schema-root-object)**.

This means that an AGSi file encoded using JSON reduces to a single object
at the top level:

``` json
    {  … everything else is in here …  }
```

!!! Note
    The term *root* will not appear in the AGSi file.
    It is merely the term used to describe the top level object.

### 2.1.2. Summary of schema

The root object contains two standalone objects:

* [agsSchema](./Standard_Root_agsSchema.md)
* [agsFile](./Standard_Root_agsFile.md)

plus the following two objects, which are the top level (parent) objects of the Project and Model groups respectively:

* [agsProject](./Standard_Project_agsProject.md)
* [agsiModel](./Standard_Model_agsiModel.md)

This is summarised diagramatically below.

![root summary UML diagram](./images/Root_summary_UML.svg)

All other objects in the schema are child objects that will be embedded in one of the above.

When using JSON encoding, the root object will look like this:

``` json
    {
      	"agsSchema": { … },
      	"agsFile": { … },
      	"agsProject": { … },
      	"agsiModel": [ … ]
    }
```

The [agsSchema](./Standard_Root_agsSchema.md) and
[agsFile](./Standard_Root_agsFile.md) objects are both **required**, i.e. mandatory.  

The other objects are optional as some or all of these may be required,
depending on the information represented.

No more than one instance of [agsProject](./Standard_Project_agsProject.md) is permitted.

More than one instance of the [agsiModel](./Standard_Model_agsiModel.md) is permitted,
e.g. an array of objects representing different models.

!!! Note
    Even if only one instance of
    [agsiModel](./Standard_Model_agsiModel.md) is required,
    it must still be encoded as an array (with one object).

### 2.1.3. Schema UML diagram

A more detailed  UML diagram for the root object and group is given below. This includes all potential child objects, so it represents the full AGSi schema (at object level).
Reference links within the schema are not shown in this diagram.

<a href="../images/Root_full_UML.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    Guidance on how to interpret the UML diagrams shown in this standard can be found in
    <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_General_UML">Guidance > General > How to read AGSi schema diagrams</a>.


![root UML diagram](./images/Root_full_UML.svg)

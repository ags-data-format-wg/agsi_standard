


# AGSi Standard / 5. Observation

## 5.7. agsiObservationColumn

### 5.7.1. Object description

An [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) object provides the data for a single column segment within an exporatory hole, i.e. value/text over a defined range of depth/elevation. Typically used for geological logging descriptions and attributes are included to facilitate this. May alternatively be used with embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects for other column segment data. Segments may be defined using depth (relative to top of parent hole) or elevation, or both.

The parent object of [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) is [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md)

[agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) contains the following embedded child objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)

[agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) has the following attributes:


- [columnID](#columnid)
- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [topElevation](#topelevation)
- [bottomElevation](#bottomelevation)
- [description](#description)
- [legendCode](#legendcode)
- [geologyCode](#geologycode)
- [geologyCode2](#geologycode2)
- [geologyFormation](#geologyformation)
- [geologyBGS](#geologybgs)
- [agsiDataPropertyValue](#agsidatapropertyvalue)
- [remarks](#remarks)


### 5.7.2. Attributes

##### columnID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this particular column observation. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``8526ef28-7a26-4c6f-b305-5e9607a7ab6d``

##### topDepth
Depth from top of parent exploratory hole to the top of the column segment. For inclined holes defined using a profile, this shall be the length measured along the line of the hole, not adjusted vertical depth.  
*Type:* number  
*Condition:* Required if *[topElevation](#topelevation)* not used  
*Example:* ``8.9``

##### bottomDepth
Depth from top of parent exploratory hole to the bottom of the column segment. For inclined holes defined using a profile, this shall be the length measured along the line of the hole, not adjusted vertical depth.  
*Type:* number  
*Condition:* Recommended if *[topDepth](# topdepth)* used  
*Example:* ``34.7``

##### topElevation
Elevation of the top of the column segment. For inclined holes, this shall be the true calculated elevation.  
*Type:* number  
*Condition:* Required if *[topDepth](# topdepth)* not used  
*Example:* ``6.3``

##### bottomElevation
Elevation of the bottom of the column segment. For inclined holes, this shall be the true calculated elevation.  
*Type:* number  
*Condition:* Recommended if *[topElevation](#topelevation)* used  
*Example:* ``-28.4``

##### description
Geological description.  
*Type:* string  
*Example:* ``Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (GOTHAM CLAY)``

##### legendCode
Legend code. Recommend using code from AGS format ABBR code list.  
*Type:* string ([code based on AGS ABBR recommended](./Standard_General_Formats.md#1611-ags-abbr-codes-project-codes))  
*Example:* ``201``

##### geologyCode
Geology code. Typically a project specific code defined defined using [agsProjectCode](./Standard_Project_agsProjectCode.md).  
*Type:* string (project [code based on AGS ABBR recommended](./Standard_General_Formats.md#1611-ags-abbr-codes-project-codes))  
*Example:* ``GC``

##### geologyCode2
2nd geology code, if applicable. Typically a project specific code defined using [agsProjectCode](./Standard_Project_agsProjectCode.md).  
*Type:* string (project [code based on AGS ABBR recommended](./Standard_General_Formats.md#1611-ags-abbr-codes-project-codes))  
*Example:* ``A2``

##### geologyFormation
Geological formation or stratum name.  
*Type:* string  
*Example:* ``Gotham Clay``

##### geologyBGS
BGS Lexicon code (applicable in UK only).  
*Type:* string (<a href="https://webapps.bgs.ac.uk/lexicon/" target="_blank">BGS Lexicon code</a>)  
*Example:* ``GC``

##### agsiDataPropertyValue
Array of embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. May be used to provide other data for this range of depth/elevation.  
*Type:* array (of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object(s))  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


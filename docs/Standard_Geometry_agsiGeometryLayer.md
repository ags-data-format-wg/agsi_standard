


# AGSi Standard / 6. Geometry

## 6.4. agsiGeometryLayer

### 6.4.1. Object description

An [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) object is a volumetric element bounded by two infinite horizontal planes at specified elevations. May be used for defining each element in a stratigraphical column (one dimensional) model. See [6.2.5. Simple 1D layers](./Standard_Geometry_Rules.md#625-simple-1d-layers-agsigeometrylayer) for interpretation.

The parent object of [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) is [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [topElevation](#topelevation)
- [bottomElevation](#bottomelevation)
- [remarks](#remarks)


### 6.4.2. Attributes

##### geometryID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this geometry object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Geometry group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``DesignPileGCC``

##### description
Short description of geometry defined here.  
*Type:* string  
*Example:* ``Design profile, pile design: Gotham Clay``

##### topElevation
Elevation (z) of the top surface. Definition of both top and bottom surfaces is recommended to minimise the risk of error.  
*Type:* number  
*Condition:* Required if *[bottomElevation](#bottomelevation)* not used  
*Example:* ``6``

##### bottomElevation
Elevation (z) of the bottom surface. Definition of both top and bottom surfaces is recommended to minimise the risk of error.  
*Type:* number  
*Condition:* Required if *[topElevation](#topelevation)* not used  
*Example:* ``-30``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


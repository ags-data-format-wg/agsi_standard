# AGSi Standard / 5. Observation

## 5.2. Observation rules and conventions

This section details rules and conventions applicable to the Observation group of objects.
It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [1.5. General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.


### 5.2.1. Usage of observations - general

General discussion of what constitutes an observation in an AGSi context is provided in
[5.1.1 Usage (Observation group)](./Standard_Observation_Intro.md#511-usage). More specific guidance is provided on this page.

Observations in AGSi should only be used to convey significant information that adds value to the model, taking into account its intended usage.

### 5.2.2. Usage of observations - GI exploratory holes and logs

Use of AGSi for exchange of only GI data, or large amounts of GI data, is not recommended.
However, it is common for ground models to include selected GI data, and the AGSi Observations group can support this.

In particular, it is common for models to include representations of exploratory holes and the geological succession found within them.
The Observations group objects
[agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) and
[agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) can be used for this.

It will commonly be the case that the geological classification and description provided is based on the
[factual data](./Standard_General_Definitions.md#factual-data),
typically from the AGS factual GEOL group. However, the following should be considered:

- Exploratory hole logs in reports generally include data from other source AGS factual groups, such as geological detailed descriptions (DETL group), groundwater observations and other depth related remarks. Exploratory hole representations in models do not always include all of this information.

- The geological classifications provided in the model may differ from those given in the source factual data. This may be due to re-interpretation of the original classification, or mapping obsolete classifications from legacy data to the project system.

- The modeller may prefer to include simplified (interpreted) descriptions where full descriptions are long and detailed. This may include consolidation of adjacent log sections with the same geological classification.

If any of the above apply to the AGSi data provided, and/or there are other matters that should be brought to the users attention, then it is recommended that details are provided within the
[AGSi package](./Standard_General_Definitions.md#agsi-package).
A brief summary should be included in the
[AGSi file](./Standard_General_Definitions.md#agsi-file) itself (use of
*[remarks](./Standard_Observation_agsiObservationSet.md#remarks)* attribute in
[agsiObservationSet](./Standard_Observation_agsiObservationSet.md)
is recommended) with full details provided in supporting
[documents](./Standard_General_Definitions.md#agsi-document)
if necessary.

[agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) can, if required, be used to convey other data with a depth/elevation range (from and to) by embedding [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. Examples could be core log information (from AGS factual CORE group) or rock/soil weathering (WETH).


### 5.2.3. Usage of observations - GI test and hole data

Sometimes, models may include selected GI test data, e.g. SPT or laboratory test data.

Use of AGSi for transfer of the full factual test data, or large amounts of data is not recommended.

It is recommended that AGSi should only be used to transfer modest amounts of test data, and only when required. Such data should add value to the model, taking into account its intended usage.

!!! Note
    Caution regarding inclusion of test data arises from the following concerns:

    - Desire to avoid duplication of factual data
    - Data brought into models is often incomplete, lacking metadata and context
    - Some data brought into models may have undergone some interpretation, e.g. extrapolation of SPT N values, details of which may not be immediately apparent

    It is anticipated that best practice on inclusion of test data, or not as the case may be, will evolve over time, and future guidance will be able to reflect this.

If test data is to be included, it is recommended that the data for each exploratory hole is provided as a profile of values against depth or elevation. The profile should be defined using a single [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object embedded in the relevant [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md). If data from further tests is required, this can be provided using additional [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects ([agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) can accommodate an array of objects).

Additional hole data, i.e not depth related, can also be accommodated using [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) embedded in  [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md). In this case [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) will normally be used to provide a single value (numeric or text) rather than a profile.

### 5.2.4. Usage of observations - points and shapes

[agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) and
[agsiObservationShape](./Standard_Observation_agsiObservationShape.md)
are general purpose objects that should be able to accommodate most observation data.

Simple observation data, e.g. a measurement or a text description, can be provided within the object attributes. If structured attributed data is to be conveyed, this can be achieved by embedding [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects.

If the above are still not sufficient, it is possible to provide a link to a
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data file by embedding an
[agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)
object. Any type of file may be linked, but note the
[limitations discussed in 5.2.7. below](#527-use-of-agsidatapropertyfromfile).

Use of [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) and
[agsiObservationShape](./Standard_Observation_agsiObservationShape.md) for providing exploratory hole data is not recommended. Use of [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) and
[agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md),
which are specifically designed for this purpose, is preferred.

### 5.2.5. Use of observation sets

All observations must be grouped into sets, as only sets can be embedded within the parent [agsiModel](./Standard_Model_agsiModel.md) object.

This applies even if there is only one observation, i.e. that observation still needs to be included in a set.

More than one set may be embedded in [agsiModel](./Standard_Model_agsiModel.md), which means that sets can be organised to best suit the nature of the observations.  For example:

- A set may be used for exploratory holes from a single GI. The
[agsiObservationSet](./Standard_Observation_agsiObservationSet.md)
attributes can be used to identify the GI. More detailed GI metadata can be provided using the
[*investigationID*](./Standard_Observation_agsiObservationSet.md#investigationid) attribute to reference an
[agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object, if used.
- Observations arising from a specific survey or field study could be grouped together as a set, so that the attributes for the set can be used to provide survey/study metadata.

Geometry may be assigned to a set by embedment of an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. This should only be used for geometry related to the set as a whole, e.g. the boundary of a study area. The location and/or geometry for individual observations should be included as part of the child observation.

### 5.2.6. Depth and/or elevation (agsiObservationColumn)

[agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) is a child of
[agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) and therefore inherits the location and alignment data of the parent exploratory hole.

The column segment defined by each
[agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md)
object is located along the line of the exploratory hole and bounded by the
top and bottom depth and/or elevations provided.

The schema allows either or both of depth (from top of hole) or elevation (Z coordinate) to be provided. If both are provided, it is up to the user to ensure that the data is compatible. If there is conflict, it is up to users/specifiers to resolve this. Use of a top depth mixed with a bottom elevation, or vice versa, is not recommended.

For inclined holes, if depth is used then this is defined as the **length** measured along line of the hole, not the vertical depth below top of hole. If elevation is used for inclined holes, then this shall be the true elevation (Z coordinate).

!!! Note
    For horizontal holes it will be necessary to use depth (as length).

### 5.2.7. Use of agsiDataPropertyFromFile

It is possible to link
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data to [agsiObservationSet](./Standard_Observation_agsiObservationSet.md), [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md), [agsiObservationShape](./Standard_Observation_agsiObservationShape.md) and
[agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md).

Links to
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data only provide the location of that data, with a broad description and, if used, some version metadata. Generally, it will not be possible for users receiving the data to be able to parse or otherwise use it, unless it conforms to an agreed specification.

!!! Note
    One possible use of this functionality would be to provide a full rich data set to accompany summary/key results that have been provided using
    [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md).

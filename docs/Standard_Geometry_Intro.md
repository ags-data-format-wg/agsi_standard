# AGSi Standard / 6. Geometry

## 6.1. Geometry group overview

### 6.1.1. Usage

The Geometry group comprises objects that can be used to describe the geometry of other objects, in particular
[model elements](./Standard_General_Definitions.md#model-element).

The Geometry objects are resources that can be embedded within other schema objects, where specified in the object reference documentation. In some cases the type of Geometry object that can be used is constrained, as defined in the relevant attribute description.

Some of the Geometry objects can be embedded within other Geometry objects, as described in
[6.1.2. Summary of schema](#612-summary-of-schema).

The objects in the Geometry group represent different types of geometry:

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
  is a pointer to geometry data contained within a
  [supporting](./Standard_General_Definitions.md#agsi-supporting-files) file.
  Includes metadata for the referenced file.
  This will need to be used for most geometry, including lines, surfaces and volumes.

- [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md)
  is a volumetric element bounded by two infinite horizontal planes at specified elevations. May be used for a stratigraphical column (one dimensional) model.

- [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md)
  is an infinite horizontal plane at a specified elevation.			

- [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
  defines a volumetric element (solid) between top and/or bottom surfaces. This is a linking object between the model element and the source geometry for the surfaces, which will normally be embedded
  [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects.

- [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
  defines an area between top and/or bottom lines. It is the 2D version of
  [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
  and is typically used for cross sections.

Refer to [6.2. Geometry rules and conventions](./Standard_Geometry_Rules.md)
for more information on how the above should be used, including details of how the volumes and areas defined by
[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
and
[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
shall be interpreted.

### 6.1.2. Summary of schema

This diagram shows the objects in the Geometry group and their relationships.

![Geometry summary UML diagram](./images/agsiGeometry_summary_UML.svg)

[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md),
[agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) and
[agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md)
may be used as standalone objects, embedded in the relevant parent object
(where permitted).

[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
and
[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
can also be embedded in the relevant parent object (where permitted).
However, these are linking objects and to provide the full geometry it will be necessary to embed a further 'child' Geometry object within the original Geometry object.
This will usually be a
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object, but
[agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) is a valid alternative for use within
[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md).

### 6.1.3. Schema UML diagram

The diagram below shows the full schema of the Geometry group including all attributes.
It also includes the direct parent and child objects of the Geometry group.
Reference links to other parts of the AGSi schema are not shown in this diagram.

<a href="../images/agsiGeometry_full_UML.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    Guidance on how to interpret the UML diagrams shown in this standard can be found in
    <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_General_UML">Guidance > General > How to read AGSi schema diagrams</a>.


![Geometry full UML diagram](./images/agsiGeometry_full_UML.svg)

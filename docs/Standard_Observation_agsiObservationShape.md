


# AGSi Standard / 5. Observation

## 5.5. agsiObservationShape

### 5.5.1. Object description

An [agsiObservationShape](./Standard_Observation_agsiObservationShape.md) is an observation related to a geographic shape, e.g. line, area, volume or series of points, defined in 2D or 3D space as required. Observations can be text or numeric value(s). Alternatively, a single [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) object can be used to provide attributed data using embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. For observations that relate to a line, area, volume or series of points, use [agsiObservationShape](./Standard_Observation_agsiObservationShape.md). For GI exploratory holes and their data, use [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md).

The parent object of [agsiObservationShape](./Standard_Observation_agsiObservationShape.md) is [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)

[agsiObservationShape](./Standard_Observation_agsiObservationShape.md) contains the following embedded child objects: 

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)

[agsiObservationShape](./Standard_Observation_agsiObservationShape.md) has the following attributes:


- [observationID](#observationid)
- [observationNature](#observationnature)
- [madeBy](#madeby)
- [date](#date)
- [observationText](#observationtext)
- [agsiGeometryFromFile](#agsigeometryfromfile)
- [agsiDataPropertyValue](#agsidatapropertyvalue)
- [agsiDataPropertyFromFile](#agsidatapropertyfromfile)
- [remarks](#remarks)


### 5.5.2. Attributes

##### observationID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this observation. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it. If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for observationID should be unique at least within each observation set ([agsiObservationSet object](./Standard_Observation_agsiObservationSet.md) object), and preferably unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``FieldGeology/Obs/001``

##### observationNature
Description of the nature of the observation (use *[observationText](#observationtext)* for the observation itself).  
*Type:* string  
*Example:* ``Geological field survey of visible outcrop``

##### madeBy
Name and/or organisation of person(s) making the observation, if applicable.  
*Type:* string  
*Example:* ``S Jones (ABC Consultants)``

##### date
Date of observation, if applicable.  
*Type:* string ([ISO date](./Standard_General_Formats.md#165-iso-date-andor-time))  
*Example:* ``2018-03-13``

##### observationText
Description of the observation as text. Not required if [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) used to provide attributed data.  
*Type:* string  
*Example:* ``Surveyed boundary of top of Gotham Clay, in side of exposed cutting.``

##### agsiGeometryFromFile
An embedded Geometry object defining the geometry for this observation, or the location of the observation, which will be a reference to an external file. The geometry may comprise: line, area, volume (or sets therefore), or a set of points.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Condition:* Required  


##### agsiDataPropertyValue
Array of embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. May be used to provide attributed data for this observation (numeric values and/or text).  
*Type:* array (of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object(s))  


##### agsiDataPropertyFromFile
An embedded [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object, which may be used to reference to an external supporting data file.  
*Type:* object (single [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


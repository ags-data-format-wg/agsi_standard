


# AGSi Standard / 6. Geometry

## 6.5. agsiGeometryPlane

### 6.5.1. Object description

An [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object is an infinite horizontal plane at a specified elevation.

[agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) may be embedded in any of the following potential parent objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
-  [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)

[agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [elevation](#elevation)
- [remarks](#remarks)


### 6.5.2. Attributes

##### geometryID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this geometry object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Geometry group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``DesignBase``

##### description
Short description of geometry defined here.  
*Type:* string  
*Example:* ``Base of design model``

##### elevation
Elevation (z) of the plane.  
*Type:* number  
*Condition:* Required  
*Example:* ``-30``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


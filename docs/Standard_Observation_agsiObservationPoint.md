


# AGSi Standard / 5. Observation

## 5.4. agsiObservationPoint

### 5.4.1. Object description

An [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) is an observation related to a single geographic point, which may be defined in 2D or 3D space as required. Observations can be text or numeric value(s). Alternatively, a single [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) object can be used to provide attributed data using embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. For observations that relate to a line, area, volume or series of points, use [agsiObservationShape](./Standard_Observation_agsiObservationShape.md). For GI exploratory holes and their data, use [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md).

The parent object of [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) is [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)

[agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) contains the following embedded child objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)

[agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) has the following attributes:


- [observationID](#observationid)
- [observationNature](#observationnature)
- [coordinate](#coordinate)
- [madeBy](#madeby)
- [date](#date)
- [observationText](#observationtext)
- [agsiDataPropertyValue](#agsidatapropertyvalue)
- [agsiDataPropertyFromFile](#agsidatapropertyfromfile)
- [remarks](#remarks)


### 5.4.2. Attributes

##### observationID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this observation. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it. If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for observationID should be unique at least within each observation set ([agsiObservationSet object](./Standard_Observation_agsiObservationSet.md) object), and preferably unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``GIHole/A/Obs/001``

##### observationNature
Description of the nature of the observation (use *[observationText](#observationtext)* for the observation itself).  
*Type:* string  
*Example:* ``GI field notes``

##### coordinate
Coordinates of the location of the observation point, as a [coordinate tuple](./Standard_General_Formats.md#168-coordinate-tuple). Coordinates provided may be 2D (x,y) or 3D (x,y,z).  
*Type:* array ([coordinate tuple](./Standard_General_Formats.md#168-coordinate-tuple))  
*Condition:* Required  
*Example:* ``[1280,2195]``

##### madeBy
Name and/or organisation of person(s) making the observation, if applicable.  
*Type:* string  
*Example:* ``J Smith (ABC Consultants)``

##### date
Date of observation, if applicable.  
*Type:* string ([ISO date](./Standard_General_Formats.md#165-iso-date-andor-time))  
*Example:* ``2018-05-18``

##### observationText
Description of the observation as text. Not required if [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) used to provide attributed data.  
*Type:* string  
*Example:* ``Original proposed location of BH01.  Hole moved due to concrete obstruction encountered at this location. ``

##### agsiDataPropertyValue
Array of embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. May be used to provide attributed data for this observation (numeric and/or text).  
*Type:* array (of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object(s))  


##### agsiDataPropertyFromFile
An embedded [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object, which may be used to reference to an external supporting data file.  
*Type:* object (single [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


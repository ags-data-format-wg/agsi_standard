


# AGSi Standard / 7. Data

## 7.4. agsiDataPropertySummary

### 7.4.1. Object description

Each [agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md) object provides a summary of data for a single defined property. Refer to [7.2. Data rules and conventions](./Standard_Data_Rules.md) for further details.

[agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md) may be embedded in any of the following potential parent objects: 

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
-  [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md) has associations (reference links) with the following objects: 

- [agsProjectCode](./Standard_Project_agsProjectCode.md)

[agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md) has the following attributes:


- [dataID](#dataid)
- [codeID](#codeid)
- [caseID](#caseid)
- [valueMin](#valuemin)
- [valueMax](#valuemax)
- [valueMean](#valuemean)
- [valueStdDev](#valuestddev)
- [valueCount](#valuecount)
- [valueSummaryText](#valuesummarytext)
- [remarks](#remarks)


### 7.4.2. Attributes

##### dataID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this data object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Data group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``42f18976-7352-4f67-9a6e-df89788343a7``

##### codeID
Code that identifies the property. Codes should be defined in either an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. The codes used by the instances of this object contained within a single parent object instance shall be unique, except that if caseID is used then only the combination of codeID and caseID needs to be unique.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Condition:* Required  
*Example:* ``TRIG_CU``

##### caseID
Code (or text) that identifies the use case for a property. See [7.2.4. Use of (data) case](./Standard_Data_Rules.md#724-use-of-data-case) for example use cases. If the input is a code, this shall be defined in an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. May be left blank or omitted, but the combination of codeID and caseID shall be unique for the instances of this object contained within a single parent object instance.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object, or text)  
*Example:* ``Clay``

##### valueMin
Minimum value.  
*Type:* number  
*Example:* ``78``

##### valueMax
Maximum value.  
*Type:* number  
*Example:* ``345``

##### valueMean
Mean value.  
*Type:* number  
*Example:* ``178.2``

##### valueStdDev
Standard deviation.  
*Type:* number  
*Example:* ``36.4``

##### valueCount
Number of results.  
*Type:* number  
*Example:* ``58``

##### valueSummaryText
Alternative text based summary, if required or preferred. May be needed when some or all values are not numeric, e.g. `<0.001`.  
*Type:* string  
*Example:* ``<0.01 to 12.57, mean 3.21, (16 results)``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


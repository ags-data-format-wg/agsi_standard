


# AGSi Standard / 7. Data

## 7.3. agsiDataPropertyValue

### 7.3.1. Object description

Each [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object provides the data for a single defined property. The property value conveyed may be numeric, a profile of numeric values (e.g. a design line) or text. Refer to [7.2. Data rules and conventions](./Standard_Data_Rules.md) for further details.

[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) may be embedded in any of the following potential parent objects: 

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
-  [agsiModelElement](./Standard_Model_agsiModelElement.md)
-  [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)
-  [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md)
-  [agsiObservationShape](./Standard_Observation_agsiObservationShape.md)
-  [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md)
-  [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md)

[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) has associations (reference links) with the following objects: 

- [agsProjectCode](./Standard_Project_agsProjectCode.md)

[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) has the following attributes:


- [dataID](#dataid)
- [codeID](#codeid)
- [caseID](#caseid)
- [valueNumeric](#valuenumeric)
- [valueText](#valuetext)
- [valueProfileIndVarCodeID](#valueprofileindvarcodeid)
- [valueProfile](#valueprofile)
- [remarks](#remarks)


### 7.3.2. Attributes

##### dataID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this data object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Data group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``42f18976-7352-4f67-9a6e-df89788343a7``

##### codeID
Code that identifies the property. Codes should be defined in either an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. The codes used by the instances of this object contained within a single parent object instance shall be unique, except that if caseID is used then only the combination of codeID and caseID needs to be unique.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Condition:* Required  
*Example:* ``TRIG_CU``

##### caseID
Code (or text) that identifies the use case for a property. See  [7.2.4. Use of (data) case](./Standard_Data_Rules.md#724-use-of-data-case) for example use cases. If the input is a code, this shall be defined in an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. May be left blank or omitted, but the combination of codeID and caseID shall be unique for the instances of this object contained within a single parent object instance.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object, or text)  
*Example:* ``Clay``

##### valueNumeric
Numeric value of a single property.  
*Type:* number  
*Example:* ``120``

##### valueText
Text value for property, if applicable.  
*Type:* string  
*Example:* ``Dry``

##### valueProfileIndVarCodeID
Code that identifies the independent variable for a profile, i.e. what the property value varies against. The code shall be defined in an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object.  
*Type:* string (Reference to *[codeID](./Standard_Project_agsProjectCode.md#codeid)* of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Example:* ``Elevation``

##### valueProfile
A profile of values as an ordered list of tuples of [independent variable value, property value]. Typically used to provide properties at different elevations. Refer to [1.6.9. Profiles or arrays of coordinate tuples](./Standard_General_Formats.md#169-profiles-or-arrays-of-coordinate-tuples) for further information.  
*Type:* array ([profile](./Standard_General_Formats.md#169-profiles-or-arrays-of-coordinate-tuples))  
*Example:* ``[[15.5,60],[14.0,75],[12.5,105]]``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


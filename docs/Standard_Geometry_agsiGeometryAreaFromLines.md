


# AGSi Standard / 6. Geometry

## 6.7. agsiGeometryAreaFromLines

### 6.7.1. Object description

An [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) object defines an element as the area between top and/or bottom lines. This will typically be used on cross sections or fence diagrams. This is a linking object between model element and the source geometry for the lines. Refer to [6.2.4. Areas from lines](./Standard_Geometry_Rules.md#624-areas-from-lines-agsigeometryareafromlines) for full details of how the area should be interpreted.

The parent object of [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) is [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) contains the following embedded child objects: 

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)

[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [agsiGeometryTop](#agsigeometrytop)
- [agsiGeometryBottom](#agsigeometrybottom)
- [remarks](#remarks)


### 6.7.2. Attributes

##### geometryID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this geometry object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Geometry group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``SectionAAGCC``

##### description
Short description of geometry defined here.  
*Type:* string  
*Example:* ``Section AA, Gotham Clay``

##### agsiGeometryTop
Geometry for top line, as embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom lines is strongly recommended to minimise the risk of error. Refer to [6.2.4. Areas from lines](./Standard_Geometry_Rules.md#624-areas-from-lines-agsigeometryareafromlines) for further details.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Condition:* Required if [*agsiGeometryBottom*](#agsigeometrybottom) not used  


##### agsiGeometryBottom
Geometry for bottom line, as embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom lines is strongly recommended to minimise the risk of error. Refer to [6.2.4. Areas from lines](./Standard_Geometry_Rules.md#624-areas-from-lines-agsigeometryareafromlines) for further details.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Condition:* Required if [*agsiGeometryTop*](#agsigeometrytop) not used  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``





# AGSi Standard / 4. Model

## 4.4. agsiModelElement

### 4.4.1. Object description

A model is made up of elements. These elements are defined by [agsiModelElement](./Standard_Model_agsiModelElement.md) objects. Each element will have embedded geometry. Which class of object is referenced will depend on the form of geometry required. Elements may also have embedded data (properties and parameters).

The parent object of [agsiModelElement](./Standard_Model_agsiModelElement.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelElement](./Standard_Model_agsiModelElement.md) contains the following embedded child objects: 

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
- [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
- [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md)
- [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md)
- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)
- [agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md)
- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)

[agsiModelElement](./Standard_Model_agsiModelElement.md) has the following attributes:


- [elementID](#elementid)
- [elementName](#elementname)
- [description](#description)
- [elementType](#elementtype)
- [geometryObject](#geometryobject)
- [agsiGeometry](#agsigeometry)
- [agsiGeometryAreaLimit](#agsigeometryarealimit)
- [agsiDataParameterValue](#agsidataparametervalue)
- [agsiDataPropertyValue](#agsidatapropertyvalue)
- [agsiDataPropertySummary](#agsidatapropertysummary)
- [agsiDataPropertyFromFile](#agsidatapropertyfromfile)
- [colourRGB](#colourrgb)
- [remarks](#remarks)


### 4.4.2. Attributes

##### elementID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for the model element. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it. If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for elementID should be unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Condition:* Recommended  
*Example:* ``GC/W``

##### elementName
Name or short description of what this element represents.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham Clay, west zone``

##### description
More verbose description, as required. Usage may be determined by type of element, e.g. for a geological unit this could be used to describe typical lithology.  
*Type:* string  
*Example:* ``Stiff to very stiff slightly sandy blue/grey CLAY, with occasional claystone layers (typically <0.1m). Becoming very sandy towards base of unit.``

##### elementType
Type of element, i.e. what the element represents in general terms.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md#931-used-in-model-group))  
*Condition:* Recommended  
*Example:* ``Geological unit``

##### geometryObject
Object type (from [Geometry group](./Standard_Geometry_Intro.md)) embedded in *[agsiGeometry](#agsigeometry)* attribute.  
*Type:* string (name of a valid Geometry object)  
*Condition:* Recommended  
*Example:* ``agsiGeometryVolFromSurfaces``

##### agsiGeometry
An embedded [Geometry group](./Standard_Geometry_Intro.md) object defining the geometry for this element. The object type referenced will depend on the type of geometry, which should be defined in *[geometryObject](#geometryobject)*. Only one object per element allowed.  
*Type:* object (from one of: [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md),  [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md), [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md), [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md), or [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md))  


##### agsiGeometryAreaLimit
If required, an embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object defining the limiting plan area for the element may be defined by reference to a closed polygon object. The polygon acts as a 'cookie cutter' so the element boundary will be curtailed to stay within the polygon. Geometry beyond the boundary is ignored. This allows a large element to be easily divided up into parts, e.g. to allow different properties or parameters to be reported for each part. Use with caution as it may not be supported by all software/applications. Confirm usage in specification. Only one object per element allowed.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  


##### agsiDataParameterValue
Array of embedded [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) objects providing parameter data specific to this model element.  
*Type:* array (of [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) object(s))  


##### agsiDataPropertyValue
Array of embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects providing discrete property data specific to this model element.  
*Type:* array (of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object(s))  


##### agsiDataPropertySummary
Array of embedded [agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md) objects providing summaries of property data specific to this model element.  
*Type:* array (of [agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md) object(s))  


##### agsiDataPropertyFromFile
An embedded [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object, which may be used to reference to an external supporting data file.  
*Type:* object (single [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object)  


##### colourRGB
Preferred display colour (RGB hexadecimal)  
*Type:* string (RGB hex colour)  
*Example:* ``#c0c0c0``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``





# AGSi Standard / 7. Data

## 7.5. agsiDataPropertyFromFile

### 7.5.1. Object description

An [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object is a pointer to data contained within an external file, such as an AGS, CSV or spreadsheet file. This object also includes metadata describing the file being referenced. Refer to [7.2.5. Limitations](./Standard_Data_Rules.md#725-limitations-of-agsidatapropertyfromfile) of [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) for further requirements and recommendations relating to this object.

[agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) may be embedded in any of the following potential parent objects: 

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
-  [agsiModelElement](./Standard_Model_agsiModelElement.md)
-  [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)
-  [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md)
-  [agsiObservationShape](./Standard_Observation_agsiObservationShape.md)
-  [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md)

[agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) has the following attributes:


- [dataID](#dataid)
- [description](#description)
- [fileFormat](#fileformat)
- [fileFormatVersion](#fileformatversion)
- [fileURI](#fileuri)
- [filePart](#filepart)
- [revision](#revision)
- [date](#date)
- [revisionInfo](#revisioninfo)
- [remarks](#remarks)


### 7.5.2. Attributes

##### dataID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this data object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Data group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``42f18976-7352-4f67-9a6e-df89788343a7``

##### description
Short description of data file defined here.  
*Type:* string  
*Example:* ``Additional data points for top of Gotham Clay from legacy boreholes, based on points marked on plan reference xxxx in report yyyy etc``

##### fileFormat
Format/encoding of the data, i.e. file format. Refer to [9.2. Vocabulary](./Codes_Vocab.md#923-used-in-data-group) for list of common formats that may be used, or provide concise identification if other format used.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md#933-used-in-data-group))  
*Condition:* Recommended  
*Example:* ``XLSX``

##### fileFormatVersion
Additional version information for file format used, if required.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``2019``

##### fileURI
[URI-reference](./Standard_General_Definitions.md#uri-reference) for the data file. This will be a relative link if file is included as part of the [AGSi package](./Standard_General_Definitions.md#agsi-package). Alternatively, a link to a location within a project document system. Spaces are not permitted in [URI-reference](./Standard_General_Definitions.md#uri-reference) strings. Refer to [ 1.6.6. URI](./Standard_General_Formats.md#166-uri) for how to handle spaces in file paths or names.  
*Type:* string ([uri-reference](./Standard_General_Formats.md#167-uri-reference))  
*Condition:* Required  
*Example:* ``data/geology/legacydata.xlsx``

##### filePart
Pointer to a specific part of a file, where required for disambiguation. For a spreadsheet file, this could be the name of the sheet used.  
*Type:* string  
*Example:* ``GothamClay``

##### revision
Revision of the referenced file.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``P3``

##### date
Date of issue of this revision.  
*Type:* string (date)  
*Example:* ``2018-10-01``

##### revisionInfo
Revision notes for this revision of the referenced file.  
*Type:* string  
*Example:* ``Minor corrections, updated for GIR rev P2.``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


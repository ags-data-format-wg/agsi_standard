


# AGSi Standard / 5. Observation

## 5.6. agsiObservationExpHole

### 5.6.1. Object description

An [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) object provides geometry and common metadata for a single exploratory hole (borehole, trial pit, CPT etc.). Further data can be provided using embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects if required. Embedded [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) objects may be used to provide a representation of the geology encountered in hole. A link to a supporting data file can be provided using an embedded [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object. 

The parent object of [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) is [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)

[agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) contains the following embedded child objects: 

- [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md)
- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)

[agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) has the following attributes:


- [holeID](#holeid)
- [holeUUID](#holeuuid)
- [holeName](#holename)
- [topCoordinate](#topcoordinate)
- [verticalHoleDepth](#verticalholedepth)
- [profileCoordinates](#profilecoordinates)
- [holeType](#holetype)
- [date](#date)
- [agsiObservationColumn](#agsiobservationcolumn)
- [agsiDataPropertyValue](#agsidatapropertyvalue)
- [agsiDataPropertyFromFile](#agsidatapropertyfromfile)
- [remarks](#remarks)


### 5.6.2. Attributes

##### holeID
[Identifier](./Standard_General_Rules.md#1510-identifiers) that is unique across the project for exploratory holes. Not necessarily the same as the original hole ID (see *[holeName](#holename)*). If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for *[holeID](#holeid)* should be unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Condition:* Recommended if *[holeName](#holename)* not used  
*Example:* ``A/BH01``

##### holeUUID
Universal/global unique identifier ([UUID](./Standard_General_Rules.md#1511-uuids)) for the hole. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity. Other attributes should be used for IDs specific to the producer and/or client (see below).  
*Type:* string ([UUID](./Standard_General_Rules.md#1511-uuids))  
*Example:* ``523ad9ed-4f75-4a55-b251-c566a8b998bd``

##### holeName
Current name or ID of the exploratory hole for general use.  
*Type:* string  
*Condition:* Recommended if *[holeID](#holeid)* not used  
*Example:* ``BH01``

##### topCoordinate
Coordinates of the top of the exploratory hole (3D, including elevation) as a [coordinate tuple](./Standard_General_Formats.md#168-coordinate-tuple).  
*Type:* array ([coordinate tuple](./Standard_General_Formats.md#168-coordinate-tuple))  
*Condition:* Required unless *[profileCoordinates](#profilecoordinates)* used  
*Example:* ``[1275.5,2195.0,15.25]``

##### verticalHoleDepth
Final depth of exploratory hole for vertical holes only. For non-vertical or non-straight holes use *[profileCoordinates](#profilecoordinates)* instead.  
*Type:* number  
*Condition:* Required unless *[profileCoordinates](#profilecoordinates)* used  
*Example:* ``25``

##### profileCoordinates
Coordinates of the line of the exploratory hole (3D, including elevation), i.e. top, bottom and intermediate changes in direction if required. Input as [ordered list of coordinate tuples](./Standard_General_Formats.md#169-profiles-or-arrays-of-coordinate-tuples) starting at the top. Used for holes that are not vertical, or not straight. May be used for straight vertical holes as alternative to *[topCoordinate](#topcoordinate)* and *[verticalHoleDepth](#verticalholedepth)*.  
*Type:* [array (of coordinate tuples)](./Standard_General_Formats.md#169-profiles-or-arrays-of-coordinate-tuples)  
*Condition:* Required if *[topCoordinate](#topcoordinate)* and *[verticalHoleDepth](#verticalholedepth)* not used  
*Example:* ``[[1275.5,2195.0,15.25], [1275.5,2195.0,-9.75]]``

##### holeType
Type of exploratory hole. Recommend using code from AGS format ABBR code list, e.g. CP+RC, with project specific codes defined using [agsProjectCode](./Standard_Project_agsProjectCode.md). Alternatively, short description may be provided, e.g. cable percussion borehole with rotary follow on.  
*Type:* string ([code based on AGS ABBR recommended](./Standard_General_Formats.md#1611-ags-abbr-codes-project-codes))  
*Condition:* Recommended  
*Example:* ``CP+RC``

##### date
Date of exploration. Recommend using start date for holes that take more than one day.  
*Type:* string ([ISO date](./Standard_General_Formats.md#165-iso-date-andor-time))  
*Example:* ``2018-05-23``

##### agsiObservationColumn
Array of embedded [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) objects which are typically used to represent geology within the hole, but can also be used for other data.   
*Type:* array (of [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) object(s))  


##### agsiDataPropertyValue
Array of embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. May be used for additional hole metadata or for profiles of test results for this hole, e.g. SPT vs depth/elevation.  
*Type:* array (of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object(s))  


##### agsiDataPropertyFromFile
An embedded [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object, which may be used to reference an external supporting data file.  
*Type:* object (single [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Original name on logs: BH1``


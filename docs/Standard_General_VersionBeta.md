# AGSi Standard / 1. General

## Version info (Beta stage only)

This page captures the revision history during the beta stage of the project, from first published release (v0.5.0) up to the issue of v1.0.0.

The final beta version was **0.6.2-beta**, published November 2022.

Version 1.0.0 is identical to 0.6.2, except for the necessary clarifying amendments to the version history pages and renaming of the JSON Schema file and schema spreadsheets.

Refer to the AGSi Gitlab 'Issue' referenced for full details and rationale of each change:
<a href="https://gitlab.com/ags-data-format-wg/agsi/AGSi_Standard/-/issues/?sort=created_date&state=all" target="_blank">https://gitlab.com/ags-data-format-wg/agsi/AGSi_Standard/-/issues</a>.


### 0.6.2-beta

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/0.6.2/" target="_blank">Click here to view</a>.

Published November 2022

#### Summary of changes

- *Standard* and *Guidance* split into separate sites, albeit still strongly linked via the menu
- Button added at top which allows use to toggle between different versions (for Standard only)
- *Codes and vocabularies* incorporated into *Standard*
- Page and section numbering added, and some pages re-titled
- Requirement for **index file** added
- URI and URI-reference should be case sensitive
- *caseID* attributes added to Data property objects
- Requirements or recommendations for uniqueness of identifiers clarified
- Correction of minor inconsistences and errors in schema documentation
- Change to JSON Schema revision numbering
- agsProjectCodeSet and agsProjectCode object descriptions clarified
- Some links added, mainly to object reference pages
- Links added to allow schema UML diagrams to open in new page
- General clarifications of wording, and correction of errors (Typos and broken links)
- Version history page cleared ready for v1.0.0. Beta stage version history moved to new page (this page!)
- JSON Schema updated


#### Detailed changes - global

- *Standard* and *Guidance* split into separate sites/repos, albeit still strongly linked via the menu (issue #116). All links and menus updated as required.
- Button added at top which allows use to toggle between different versions (issue #116)
- Numbering added to pages and sections (issue #65)
- Menu items updated for numbering, and some page titles amended (issue #155)
- Attribute descriptions for many objects in Project, Model and Observation groups amended to clarify requirements or recommendations for uniqueness of identifiers (issue #147)
- General clarifications of wording, and correction of errors,, i.e. typos and broken links. From full verification proof reading check (issue #149)
- Links added to allow schema UML diagrams to open in new page (part of issue #149)
- JSON Schema updated, using new version numbering system (issue #158, #159)

#### Detailed changes to schema / other requirements

*Codes and vocabularies* incorporated into Standard as section 9 (issue #153):

- *(Codes and vocabularies) Introduction* page deleted
- Minor changes to wording on *9.1. Code list* and *9.2. Vocabulary* pages to reflect that these are now part of Standard
- *1.1.1. Contents of this standard* updated to reflect the change
- *1.1.4. Governance* updated to reflect the change
- Links from other pages updated as required

URI and URI-reference should be case sensitive (issue #160). Relevant sections amended (*1.5.2.*, *1.6.6*, *1.6.7*).

New requirement for **index file** (issue #126):

- *1.8.1. AGSi file and package* amended to include requirement
- New section *1.8.2. Index file* provides full requirements
- Figure in *1.8.5. Recommended folder structure* updated accordingly

*caseID* attribute added to *agsiDataPropertyValue* and *agsiDataPropertySummary* (issue #141).
Schema diagrams in *1.4.3* and *7.1.3* updated accordingly.

*agsProjectCoordinateSystem* attribute *transformshiftz* renamed to *transformShiftZ* (issue #151)

Object descriptions clarified and links corrected on *agsProjectCodeSet* and *agsProjectCode* object reference pages (#issue #151).

Version history page cleared ready for v1.0.0. Beta stage version history moved to new page, i.e. this page! (Issue #161)

##### Minor changes

- Reference to Gitlab issues moved to top of this version update page, and link added.
- 1.1 Introduction minor amendments to reflect split of Standard from Guidance and version button (issue #157).
- Note added to 1.1.3 to clarify Object reference precedence over UML diagrams (issue #163) 
- Note on use of AGSi for earthworks added to 1.3.2 Scope exclusions (issue #137)
- New section heading *1.4.3. Full AGSi schema UML diagram* and minor changes to texts on this page to put more focus on the schema diagram (issue #154).
- 1.5.3 and 8.2: Additions to clarify that number data type allows scientific notation (issue #144)
- Added note to 1.8.1 about opening .agsi files as zip files (issue #160)
- Clarified links in descriptions for some Data objects (issue #156)
- JSON Schema version numbering to include version of JSON Schema applicable, e.g. 1.0.0+2019-09. Section 1.1.4 amended. (Issue #159)


### 0.6.1-beta

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/0.6.1/" target="_blank">Click here to view</a>.

Published October 2022

#### Summary of changes

- ID/UUID attributes added to some objects.
- Minor corrections/improvements as listed below.

#### Detailed changes to schema

Schema and documentation updated to accommodate the following headline changes.

- Response to Issue #120: ID attributes added to all objects that did not previously have one, with one exception as noted in detail below.
UUID attribute added where these was already an ID attribute in use. Definition of 'Identifier' updated to clarify how they are used.

- Clarifications in response to issues #135, #136, #142
- Global minor changes, including responses to Issues #123, #124, #133, #138, #140

##### ID attributes added (or reinstated) for many objects

New attributes added to:

- *projectUUID* in agsProject (this must be a UUID)
- *documentID* in agsProjectDocument
- *boundaryID* in agsiModelBoundary
- *holeUUID* in agsiObservationExpHole (this must be a UUID)
- *columnID* in agsiObservationColumn
- *geometryID* in all Geometry group objects (reinstated)
- *dataID* in all Data group objects

!!! Note
    NOT added for agsProjectCode. This is because there is already a codeID that has a specific use and we would be adding a new code that looks very similar (but a UUID) for potential future version control reasons only. This would probably just cause confusion, for limited benefit, so we will leave it out, at least for now.

In addition:

- descriptions for identifier attributes updated for clarity and consistency
- identifier or UUID, as required, added to 'additional requirements' for ID attributes where previously missing
- *Identifiers* section of *Standard/General/Rules* page updated to discuss identifiers that are optional

##### Other clarifications

New section *Limitations of agsiDataPropertyFromFile* on *Data rules and conventions* page (Issue #125).

One Project per AGSi package (Issue #135), on pages:

- *agsProject* page: object description
- *Usage and schema overview*: *Projects and investigations* section
- *Project rules and conventions* *Projects and investigations* section

Spaces not permitted in URI (Issue '#136'), on pages:

- *Data input formats*: *URI* and *URI_reference* sections
- Clarifying note appended to attribute description for all URI/URI-reference type/format attributes

Identifiers and UUID are not case sensitive (Issue #142), on pages:

- *General rules and conventions*: *Case sensitivity*, *Identifiers* and *UUIDs* sections

- *agsProjectCode* attribute description corrected to reflect that it should now include parameter and property codes used by Data Objects. Example data also amended.

##### Global minor changes

*date* in data type additional requirements changed to *ISO date* throughout documentation.

Remaining references to *data set* reviewed and corrected where applicable.

Some internal links added or corrected.

Some minor formatting updates or corrections.

Some minor typos or other errors corrected.

Some minor errors on diagrams corrected.

Redundant menu items removed.


### 0.6.0-beta

This version is no longer available to view.

Published November 2021

#### Summary of changes

Schema and documentation updated to accommodate the following headline changes.

- schema and file objects (in root) renamed agsSchema and agsFile (Issue #114)
- Geometry objects now embedded in agsiModel (Issue #97)
- Geometry to be mostly in external files now, allowing some geometry objects to be deleted (Issue #102)
- Data objects now embedded in agsiModel/agsiProject (response to Issue #38)
- Data objects rationalised and to suit the above, and some new data objects added (Issue #111, Issue #99)
- Boreholes incl geology removed from Geometry, now incorporated in new Observation group. This new group also includes objects for general observations (Issue #99)
- Section alignments supported using new agsiModelAlignment object (Issue #107)
- agsiModelSubset deleted (Tony 27/4/21 email and subsequent meeting)
- agsProjectCode *code* attribute changed to *codeID* (Issue #117)
- Data type *string (uri)* changed to *string (uri-reference)* where relative references are permitted (Issue #66)
- Requirements for transfer of AGSi revised (Issue #108)

#### Detailed changes to schema

!!! Note
    In the schema spreadsheets, red is used to identify changes and additions, but deleted rows/cells are not identified (they are just missing!).

##### Global minor changes

- In the examples, all References to Gotham City Clay (GCC) changed to Gotham Clay (GC).
- Some minor amendments to descriptions and examples, for clarity and/or consistency
- Typographical corrections affecting descriptions and examples only
- Broken links repaired (where found)
- Some new links added

##### Root

- *agsiGeometry* and *agsiData* attributes deleted (Geometry and Data groups are no longer children of root as per changes outlined below)
- *schema* attribute renamed *agsSchema*
- *file* attribute renamed *agsFile*
- *system URI* attribute in file/agsFile deleted (duplicate of fileURI)


##### Model group

agsiModel:

- NEW attribute: *alignmentID*.  
  Reference to ID of an agsiModelAlignment object. Used by models that are 2D sections to identify the alignment of the section.
- NEW attribute:  *agsiObservationSet*.
  Embedded agsiObservationSet objects - new objects, described below. Used for sets of observations, incorporating exploratory holes and their geology.
- NEW attribute: *agsiModelAlignment*.
  Embedded agsiModelAlignment object - described below. Used to define the alignments themselves in the relevant primary model.

agsiModelAlignment (NEW):

New object, a child of agsiModel, with the following attributes:
- *alignmentID*: Identifier that may be referenced by alignmentID attribute of agsiModel
- *alignmentName*: Short name, e.g. Section AA
- *description*: Further description if required.
- *agsiGeometry*: Geometry of alignment as a 2D line in XY space. This will normally be an embedded agsiGeometryFromFile object pointing to a line defined in a GeoJSON or WKT file.
- *startChainage*: The section chainage/baseline distance for the first point defined on the alignment. Assumed to be zero if not used.
- *remarks*: As usual

agsiModelSubset (DELETED):

- Object deleted. Includes deletion of *subsetID* attribute in agsiModelElement, and *agsiModelSubset* attribute in agsiModel, and update of object description to suit.

agsiModelElement:

- *geometryID* attribute replaced with *agsiGeometry* containing embedded Geometry object
- *geometryObject* description updated to reflect the above
- *areaLimitGeometryID* attribute replaced with *agsiGeometryAreaLimit* containing embedded Geometry object
- *parameterSetID* attribute replaced by *agsiDataParameterValue* containing array of embedded Data objects
- *propertySetID* attribute replaced by *agsiDataPropertyValue* containing array of embedded Data objects
- NEW attribute *agsiDataPropertySummary* containing array of embedded Data objects, now to be used for summary data in preference to *agsiDataPropertyValue*
- NEW attribute *agsiDataPropertyFromFile* containing array of embedded Data objects, which may be used to reference external supporting data files

- object description updated to reflect the above

agsiModelBoundary:

- *boundaryXYGeometryID* replaced by *agsiGeometryBoundaryXY* containing embedded agsiGeometyFromFile object
- *topSurfaceGeometryID* replaced by *agsiGeometrySurfaceTop* containing embedded agsiGeometyFromFile object
- *bottomSurfaceGeometryID* replaced by *agsiGeometrySurfaceBottom* containing embedded agsiGeometyFromFile object
- object description updated to reflect the above

##### Observation group (NEW)

This is a new group, providing resources for agsiModel (see above).

The group comprises a parent object, **agsiObservationSet**, which contains arrays of the following child objects:

- agsiObservationPoint
- agsiObservationShape
- agsiObservationExpHole, which has a child:
    -  agsiObservationColumn

Refer to documentation for full details and attributes.

##### Geometry group

agsiGeometry (DELETED):

- Object deleted. No longer required as all geometry now embedded in agsiModelSubset (for now - may change in long term!)

agsiGeometryExpHoleSet, agsiGeometryExpHole, agsiGeometryColumnSet, agsiGeometryColumn (DELETED):

- All of these objects deleted, replaced by new objects: agsiObservationSet, agsiObservationExpHole and agsiObservationColumn (see above).

agsiGeometryLine (DELETED):

- Object deleted. It is now recommended that lines should be defined using external files, e.g. GeoJSON or WKT.

agsiGeometryFromFile

- *geometryID* deleted - no  longer required
- *fileURI* data type changed to 'string (uri-reference)'
- *fileLayer* renamed to *filePart*

agsiGeometryVolFromSurfaces, agsiGeometryAreaFromLines, agsiGeometryPlane, agsiGeometryLayer:

- *geometryID* deleted - no  longer required


##### Data group

agsiData (DELETED):

- Object deleted. No longer required as all data now embedded in the relevant objects (for now - may change in long term!)

agsiDataParameterSet, agsiDataPropertySet (DELETED):

- Both objects deleted. No longer required as all data value objects now embedded directly in the relevant objects as an array (for now - may change in long term!)

agsiDataCode, agsiDataCase (DELETED):

- Both objects deleted. agsProjectCode (and agsProjectCodeSet) are now to be used instead.

All references in Data group to agsiDataCode and agsiDataCase updated to reference to agsiProjectCode and agsiProjectCodeSet as required.

agsiDataPropertyValue:

- New attribute *valueNumber*
- *caseID* attribute deleted
- The following attributes are deleted (moved to new *agsiDataPropertySummary* object)
  - *valueMin*, *valueMax*, *valueMean*, *valueStdDev*, *valueCount*

agsiDataPropertySummary (NEW):

New object containing the following attributes (moved from agsiDataPropertyValue):

- *codeID*, *valueMin*, *valueMax*, *valueMean*, *valueStdDev*, *valueCount*
- *valueSummaryText* (former *valueText* renamed)
- *remarks*

agsiDataPropertyFromFile (NEW):

New object, that can be used to provide a reference and link to a supporting data file.
This object will be accessible by (i.e. a child of) agsProjectInvestigation, agsiModelElement and most of the Observation objects. Object attributes identical to those of agsiGeometryFromFile, except for geometryType which is not applicable here.


##### Project group

agsProjectCodeSet:

- Object description and some attribute descriptions amended to reflect absorption of agsiDataCode and agsiDataCase
- Example data changed to use agsiDataParameterValue codes
- *sourceURI* data type changed to string (uri-reference)

agsProjectCode:

- attribute name *code* changed to *codeID*
- NEW attributes *units* and *isStandard*, to provide information that was previously in agsiDataCode

agsProjectDocument:

- *documentURI* data type changed to string (uri-reference)

agsProjectInvestigation:

- *propertySetID* attribute replaced with *agsiDataPropertyValue* containing array of embedded Data objects, and object description amended accordingly


#### Detailed changes to documentation

##### New, deleted and renamed pages

NEW section for new Observation group, which has NEW pages:

- Introduction
- Rules and Conventions
- Object reference pages for all Observation group objects

NEW object reference pages for new objects in other groups:

- agsiModelAlignment
- agsiDataPropertySummary
- agsiDataPropertyFromFile

Pages renamed due to objects renamed:

- schema to agsSchema, file to agsFile

Pages deleted due to objects deleted:

- agsiModelSubset
- agsiGeometry, agsiGeometryExpHoleSet, agsiGeometryExpHole, agsiGeometryColumnSet, agsiGeometryColumn, agsiGeometryLine
- agsiData, agsiDataParameterSet, agsiDataPropertySet, agsiDataCode, agsiDataCase

Geometry / Geometry object summary page deleted. Content consolidated into Geometry / Introduction.

Menu items amended to reflect new and deleted pages.

##### Other general changes

Content significantly revised to take into account the schema changes. Pages affected:

- Usage and schema overview (for Project, Model, Geometry and Data)
- Rules and conventions (for Project, Model, Geometry and Data)
- General / Scope
- General / Schema
- General / Rules and conventions
- General / Data input formats
- General / Specification

Other changes to other pages:

General / Definitions:  
Definitions for the following added, amended or deleted:

- *AGSi file*, replaces *(AGSi) file*
- *AGSi package*, replaces *(AGSi) file/data set*
- *(AGSi) document*
- *Complementary data* deleted
- *Factual data*
- *(AGSi) included files*
- *Observation* (new)
- *(AGSi) supporting files*
- *URI-reference* (new)

General / Transfer of AGSi files:  
Re-written to reflect amended requirements. Includes clarification to terminology used.

Encoding / JSON Schema:  
Updated with link to updated JSON Schema file. Notes added referencing to JsonSchema version issue and JSON Type Definition.

References throughout to the AGSi file and file/data set have been reviewed and amended as required to fit with the new methodology and terminology described in
[General / Transfer](./Standard_General_Transfer.md). Typically, *AGSi file/data set* replaced with *AGSi package*.

References to URI modified to correctly differentiate between URI and URI-reference.  The following pages have been updated accordingly:

- Standard/General/Definitions
- Standard/General/Data input formats
- Standard/General/Transfer of AGSi
- Standard/Project/Rules and conventions
- Standard/Geometry/Usage and schema overview
- Standard/General/General rules and conventions



### 0.5.0-beta

This version is no longer available to view.

Published November 2020.

First public release.

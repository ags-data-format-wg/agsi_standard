


# AGSi Standard / 4. Model

## 4.3. agsiModel

### 4.3.1. Object description

An [agsiModel](./Standard_Model_agsiModel.md) object is the parent object for a single model. It contains general metadata for a model as well as the embedded element and boundary objects that make up the model. It may also contain embedded sets of observation objects, which can be used to represent exploratory holes, their geology and other data, as well as general observations. [agsiModel](./Standard_Model_agsiModel.md) may also include the alignments of related sections (the sections themselves willl normally be separate models).  There can be several models, each defined by an [agsiModel](./Standard_Model_agsiModel.md) object, in an [AGSi file](./Standard_General_Definitions.md#agsi-file).

The parent object of [agsiModel](./Standard_Model_agsiModel.md) is [root](./Standard_Root_Intro.md)

[agsiModel](./Standard_Model_agsiModel.md) contains the following embedded child objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
- [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md)
- [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)

[agsiModel](./Standard_Model_agsiModel.md) has associations (reference links) with the following objects: 

- [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)
- [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md)

[agsiModel](./Standard_Model_agsiModel.md) has the following attributes:


- [modelID](#modelid)
- [modelName](#modelname)
- [description](#description)
- [coordSystemID](#coordsystemid)
- [modelType](#modeltype)
- [category](#category)
- [domain](#domain)
- [input](#input)
- [method](#method)
- [usage](#usage)
- [uncertainty](#uncertainty)
- [documentSetID](#documentsetid)
- [alignmentID](#alignmentid)
- [agsiModelElement](#agsimodelelement)
- [agsiModelBoundary](#agsimodelboundary)
- [agsiModelAlignment](#agsimodelalignment)
- [agsiObservationSet](#agsiobservationset)
- [remarks](#remarks)


### 4.3.2. Attributes

##### modelID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for the model. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it. If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for modelID should be unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Condition:* Recommended  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

##### modelName
Short name of model.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Sitewide geological model``

##### description
More verbose description of model, if required.  
*Type:* string  
*Example:* ``C999 Package X Sitewide geological model exported from SomeGeoModelSoftware. Incorporates 2018 GI data ``

##### coordSystemID
Reference to coordinate system applicable to this model (relevant [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object).  
*Type:* string (reference to *[systemID](./Standard_Project_agsProjectCoordinateSystem.md#systemid)* of  [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object)  
*Example:* ``MetroXYZ``

##### modelType
[Type](./Standard_General_Definitions.md#type-of-model) of model. Incorporates [domain](./Standard_General_Definitions.md#domain-of-model) and [category](./Standard_General_Definitions.md#category-of-model) of model.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md#931-used-in-model-group), or appropriate combination of [domain](./Standard_General_Definitions.md#domain-of-model) and [category](./Standard_General_Definitions.md#category-of-model) terms)  
*Condition:* Recommended  
*Example:* ``Geological model``

##### category
[Category](./Standard_General_Definitions.md#category-of-model) of model.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md#931-used-in-model-group))  
*Example:* ``Observational``

##### domain
[Domain](./Standard_General_Definitions.md#domain-of-model) of model.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md#931-used-in-model-group))  
*Example:* ``Engineering geology``

##### input
Short description of input data used by model an/or cross reference to document describing this.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Input data described in GIR section 3.3.2``

##### method
Short description of method used to create model, including software used, and/or reference to the document where this is discussed.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``3D model created in SomeGeoModelSoftware. See GIR section 3.3.3 for details.``

##### usage
Short description of intended and/or permitted usage including limitations or restrictions. Strongly recommended.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Observational and interpolated geological profile. For reference and visualisation only. Not suitable for direct use in design. See GIR section 3.3.4 for details.``

##### uncertainty
Short statement discussing uncertainty with respect to the information presented in this model.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``The boundaries of the geological units presented in this model are a best estimate based on interpolation between exploratory holes, which in some cases are >100m apart. In addition, in some places the boundaries are based on interpretation of CPT results. Therefore the unit boundaries shown are subject to uncertainty, which increases with distance from the exploratory holes. Refer to GIR section 3.3.4 for more information. ``

##### documentSetID
Reference to documentation relating to model (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object).  
*Type:* string (reference to *[documentSetID](./Standard_Project_agsProjectDocumentSet.md#documentsetid)* of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``ExampleDocSetID``

##### alignmentID
Reference to ID of an [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) object found in another model. Required by 2D section models to identify the alignment of the section.  
*Type:* string (reference to *[alignmentID](./Standard_Model_agsiModelAlignment.md#alignmentid)* of [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) object)  
*Example:* ``sectionAA``

##### agsiModelElement
Array of embedded [agsiModelElement](./Standard_Model_agsiModelElement.md) objects.  
*Type:* array (of [agsiModelElement](./Standard_Model_agsiModelElement.md) objects)  


##### agsiModelBoundary
Single embedded [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object.  
*Type:* object (single [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object)  


##### agsiModelAlignment
Array of embedded [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) objects. Used to define the (section) alignments in this (primary) model object.  
*Type:* array (of [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) objects)  


##### agsiObservationSet
Array of embedded [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) objects.  
*Type:* array (of [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) objects)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``


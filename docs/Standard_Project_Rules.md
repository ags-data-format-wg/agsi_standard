# AGSi Standard / 3. Project

## 3.2. Project rules and conventions

This section details rules and conventions applicable to the Project group of objects.
It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [1.5. General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.


### 3.2.1. Projects and investigations

For the purposes of AGSi, the [Project](./Standard_General_Definitions.md#project)
is the specific project/commission under which this AGSi has been delivered.

Only one [Project](./Standard_General_Definitions.md#project) may be defined
for each [AGSi package](./Standard_General_Definitions.md#agsi-package).

The [Project](./Standard_General_Definitions.md#project) is most likely to be just one component of an
[ultimate project](./Standard_General_Definitions.md#ultimate-project),
typically the works that are eventually to be constructed, that will benefit from the
[Project](./Standard_General_Definitions.md#project).
Alternatively the [ultimate project](./Standard_General_Definitions.md#ultimate-project)
may be a framework contract set up by an asset owner.

In some cases the [Project](./Standard_General_Definitions.md#project) will be directly procured by the
[ultimate project](./Standard_General_Definitions.md#ultimate-project). However, in some cases the
[Project](./Standard_General_Definitions.md#project)
may be procured under an intermediate
[parent project](./Standard_General_Definitions.md#parent-project). Sometimes there may be further tiers of parent project.

Regardless of the relationship between the
[Project](./Standard_General_Definitions.md#project),
[parent project(s)](./Standard_General_Definitions.md#parent-project) and
[ultimate project](./Standard_General_Definitions.md#ultimate-project),
the [Project](./Standard_General_Definitions.md#project)
for the purposes of AGSi will be as defined above. Information on the
[ultimate project](./Standard_General_Definitions.md#ultimate-project)
and, if applicable, [parent project(s)](./Standard_General_Definitions.md#parent-project) should be provided as part of the metadata for the
[Project](./Standard_General_Definitions.md#project).

An [investigation](./Standard_General_Definitions.md#ground-investigation)
is a campaign of fieldwork and/or
laboratory testing carried out and reported under a site/ground investigation contract.
An [investigation](./Standard_General_Definitions.md#ground-investigation) will itself be a project, and in some cases
this may be the [Project](./Standard_General_Definitions.md#project)
under which the AGSi has been delivered.
However, it will commonly be the case that the AGSi ground models and/or interpreted data are based on data from multiple
[investigation(s)](./Standard_General_Definitions.md#ground-investigation),
which may or may not be contractually linked to the
[Project](./Standard_General_Definitions.md#project).
Therefore the schema separates the
[Project](./Standard_General_Definitions.md#project) (for AGSi) from the [investigation(s)](./Standard_General_Definitions.md#ground-investigation)
that the AGSi is based on. The
[AGSi file and package](./Standard_General_Definitions.md#agsi-file)
should include information on both.

If an investigation is also the [Project](./Standard_General_Definitions.md#project), it is recommended that full details be provided in both the
[agsProject](./Standard_Project_agsProject.md) and the  [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) objects.


### 3.2.2. Coordinate systems

#### 3.2.2.1. Types of coordinate system supported

AGSi supports only cartesian systems. The following variants may be used:

- XYZ (3D)
- XZ (2D vertical section)
- XY (2D map)
- Z (elevation only, e.g. simple 1D layer profiles).

Decimal units of measure must be used. Use of latitude and longitude (degrees/minutes/second) is not directly  supported by AGSi.

For XYZ and XY systems, the units of measure used for the X and Y axis must be the same.

#### 3.2.2.2. Model coordinate system(s)

The coordinate system used by a model is considered to be the [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system), although this could be an established regional or national system.

If all models in an
[AGSi file](./Standard_General_Definitions.md#agsi-file)
share the same system, then this system only needs to be defined once in the
[AGSi file](./Standard_General_Definitions.md#agsi-file).
If this is the case, the single
[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
object must still be included within an array given that the parent attribute data type is array, in accordance with
[1.5. General rules and conventions](./Standard_General_Rules.md#arrays).
Replacing the array with a single object is not permitted.

The coordinate system(s) to be used for the project should be described in the
[specification](./Standard_General_Specification.md).

#### 3.2.2.3. Global coordinate system transformation

A secondary [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system), which will normally be an established regional or national system, may also be defined. This will only exist via transformation from the [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system).

Usage of the attributes for transformation is defined in the [agsProjectCoordinateSystem object reference](./Standard_Project_agsProjectCoordinateSystem.md).
The diagram below provides further clarification:

![Coordinate transformation diagram](images/Project_coordinatetransform.svg)

### 3.2.3. Documents

The allocation of [documents](./Standard_General_Definitions.md#document) into sets
([agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) objects)
must take account of the references to sets of documents required from the relevant parts of the schema.

If a document is part of more then one set, the relevant
[agsProjectDocument](./Standard_Project_agsProjectDocument.md)
object needs to be repeated in each [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md).

!!! Note
    The [agsProjectDocument](./Standard_Project_agsProjectDocument.md)
    object only provides a link to the document file, not the file itself.
    Therefore the document does not have to be provided multiple times as the links in different
    [agsProjectDocument](./Standard_Project_agsProjectDocument.md)
    objects can point to the same document file.

If there is only one document in a set, then the
[agsProjectDocument](./Standard_Project_agsProjectDocument.md)
object must be included within an array given that the parent attribute data type is array, in accordance with
[1.5. General rules and conventions](./Standard_General_Rules.md#arrays).
Replacing the array with a single object is not permitted.

Inclusion of link(s) to the document file is recommended.

If the document is not included as part of the
[AGSi package](./Standard_General_Definitions.md#agsi-package)
(with a [URI-reference](./Standard_General_Definitions.md#uri-reference)
relative link provided), then a [URI](./Standard_General_Definitions.md#uri) link to an [external](./Standard_General_Definitions.md#agsi-external-files)
resource, typically a project document management system, should be included. However, this should only be done when the intended users of the file have access to that external resource.

Files used for geometry (referenced by
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects) and files referenced by [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)
do not need to be included as documents.

Project requirements for documents, in particular the use of links, should be described in the
[specification](./Standard_General_Specification.md).

### 3.2.4. Codes for Data objects

For the codes used by the [Data group](./Standard_Data_Intro.md) for
[properties](./Standard_General_Definitions.md#property) and
[parameters](./Standard_General_Definitions.md#parameter),
use of codes given in
[9.1. Code list](./Codes_Codelist.md) is recommended.

Inclusion of each code as individual
[agsProjectCode](./Standard_Project_agsProjectCode.md) objects is optional when standard codes from [9.1. Code list](./Codes_Codelist.md)
are used, provided that use of the AGSi code list is identified in
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md).
Any non standard codes required shall be included as
[agsProjectCode](./Standard_Project_agsProjectCode.md) objects, with the
[*isStandard*](./Standard_Project_agsProjectCode.md#isstandard) attribute set as `false`.

### 3.2.5. Codes where use of AGS ABBR recommended

For attributes where the specified data type and format is *string (code based on AGS ABBR recommended)*
it is anticipated that that the codes may correspond to
<a href="https://www.ags.org.uk/data-format/ags4-data-format/" target="_blank">ABBR codes inherited from AGS factual data</a>.
Re-use of codes from the AGS factual data, if applicable, is not mandatory. However, doing so should facilitate more efficient data transfer.

If AGS ABBR codes are not used, it is recommended that the codes adopted should differ significantly from the AGS ABBR codes to avoid potential confusion.
For example use of `TrialPit` would be preferable to `TP`.

Inclusion of each code as individual
[agsProjectCode](./Standard_Project_agsProjectCode.md) objects is optional when
<a href="https://www.ags.org.uk/data-format/ags4-data-format/" target="_blank">standard ABBR codes as published on the AGS website</a>
are used, provided that use of the AGS ABBR list is identified in
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md).

Use of the
<a href="https://www.ags.org.uk/data-format/ags4-data-format/" target="_blank">AGS ABBR list</a>
is recommended. Use of an established alternative list, such as a project specific dictionary on a major project or an alternative published list, is permitted. In such cases, inclusion of each code as an individual [agsProjectCode](./Standard_Project_agsProjectCode.md) objects is recommended. However this may be omitted if the list is identified and linked in
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md)
and users will always have access to the relevant resource.

Project requirements for codes should be described in the
[specification](./Standard_General_Specification.md).

# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 18:14:03 2019
Modified and turned into sub 23/11/20

@author: neil.chadwick
"""

def replaceObjectLink(originaltxt,replacementlist):
    
    # This sub is a replacement tool specifically geares towards creating links
    # based on simple text. Written to create links for objects by replacing, e.g 
    # agsiData with the text for the full link
    # Target is individual words, e.g. for target aaa, then aaa will be replaced
    # by bbaaabb will reamin untouched. Also, if target is precedded by " or # 
    # it will remain untouched. I think there may be other exceptions to 
    # (the regex is the key to this)
    # See other sub if you just want a 'simple' replacement (but use that 
    # one with extreme care!)
    # Input = csv file as a list of: 'target string','replacement string' 
    
    import re

       
    #Read files to be processed
    replacetxt = ''
    replacetextlines = []
    
     
    # old - for full file
    #Do replacements, loop through each word in list of replacements
    #Note that we need to pad word with a space becuase need to capture # before
    # (not elegent, but seems to work)
    # May be problem - won't reconginse [ etc!]
    #for replaceword in replacementlist:
    #    replaceword[0] = ' ' + replaceword[0]
    #    replaceword[1] = ' ' + replaceword[1]
    #    regexfind = r'(?<!\[|\#|\"|/|\(){}\b'.format(replaceword[0])
    #    regexreplace =  replaceword[1]  
    #    replacetext = re.sub(regexfind, regexreplace, replacetext)   
    
    # Try spltting into ine by line so we can intercept title lines
    
    for replacetextline in originaltxt.split('\n'):
       
        #Do replacements, loop through each word in list of replacements
        # Check line does not start with # or Example: - if it does, ignore
            
        if replacetextline== None:
            replacetextlines = replacetextlines.append('\n') 
        elif replacetextline.startswith('#'):
            replacetextlines.append(replacetextline)
        elif replacetextline.startswith('*Example:'):
            replacetextlines.append(replacetextline)
        else:
            for replaceword in replacementlist:
                #regexfind = r'(?<!\[|\#|\"|/|\(){}\b'.format(replaceword[0])
                regexfind = r'(?<!\[|\#|\"|/|\(|_){}\b'.format(replaceword[0])
                regexreplace =  replaceword[1]  
                replacetextline = re.sub(regexfind, regexreplace, replacetextline)     
            replacetextlines.append(replacetextline)      
        
    #return new text
    replacetxt = replacetxt + '\n' + '\n'.join(replacetextlines)
    return replacetxt


### This is the simple version - strang text strng replacement, no if no buts
### Use with caution!

def replaceSimple(originaltxt,replacementlist):
    
    #  Simple version - simply text strings
    
      
    #Read files to be processed
    replacetxt = ''
    replacetextlines = []
    
            
 # Try splitting into line by line so can intercaept title lines
    
    for replacetextline in originaltxt.split('\n'):
       
        #Do replacements, loop through each entry in list of replacements
        # Check line does not start with # - if it does, ignore
            
        if replacetextline== None:
            replacetextlines = replacetextlines.append('\n') 
        elif replacetextline.startswith('#'):
            replacetextlines.append(replacetextline) 
        else:
            for replacestring in replacementlist:
                newtext = replacetextline.replace(replacestring [0],replacestring [1])
                replacetextline = newtext
            replacetextlines.append(replacetextline)      

        
    #return new text
    replacetxt = replacetxt + '\n' + '\n'.join(replacetextlines)
    return replacetxt

###### Main excecution when running standalone file replacement 

def temp():

    from tkinter import Tk, filedialog
    import os
    import shutil
    import csv
    
    #Get input file
    root = Tk()
    inputfile=filedialog.askopenfilename(initialdir = '',
                                        title = 'Select input file',
                                        filetypes = [('Markdown file','*.md'),
                                                     ('Text files','*.txt')])
    #Correct file name hard way until fix found
    inputfile = inputfile.replace('/','\\')
    inputfolder = os.path.dirname(inputfile)
    
    
    #Point to locations where list of object replacements found (CSV)
    objectreplacementfile=filedialog.askopenfilename(initialdir = '',
                                        title = 'Select text/csv file with obkect link replacements',
                                        filetypes = (('csv','*.csv'),('Text files','*.txt')))
    
    root.destroy()
    
    # Get text from inputfile
    f = open(inputfile, 'r', encoding ='utf-8-sig')
    replacetext = f.read()  # This reads whole file
    f.close
    
    # Parse replacement list to array to be used
    with open(objectreplacementfile,encoding ='utf-8-sig',newline='') as csvfile:
        objectreplacementlist = list(csv.reader(csvfile))
    
    # Do the replacement using function
    outputtxt = replaceObjectLink(replacetext,objectreplacementlist) 
    
    # Get backup file just in case of disaster
    backupfile = inputfile + '.bak'
    shutil.copy(inputfile,backupfile)
    
    # Write new text back to original file
    f = open(inputfile, 'w')
    f.write(outputtxt)
    f.close()
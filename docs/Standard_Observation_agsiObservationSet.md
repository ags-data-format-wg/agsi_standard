


# AGSi Standard / 5. Observation

## 5.3. agsiObservationSet

### 5.3.1. Object description

An [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) object is a user defined set of observations. The individual observations are included as embedded objects [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md), [agsiObservationShape](./Standard_Observation_agsiObservationShape.md) and [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md). For exploratory holes, a set may typically correspond to the holes from a particular ground investigation. For other types of observations, users/specifiers may decide how best to group the observations.

The parent object of [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiObservationSet](./Standard_Observation_agsiObservationSet.md) contains the following embedded child objects: 

- [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md)
- [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md)
- [agsiObservationShape](./Standard_Observation_agsiObservationShape.md)
- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)

[agsiObservationSet](./Standard_Observation_agsiObservationSet.md) has associations (reference links) with the following objects: 

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agsiObservationSet](./Standard_Observation_agsiObservationSet.md) has the following attributes:


- [observationSetID](#observationsetid)
- [description](#description)
- [investigationID](#investigationid)
- [documentSetID](#documentsetid)
- [agsiObservationExpHole](#agsiobservationexphole)
- [agsiObservationPoint](#agsiobservationpoint)
- [agsiObservationShape](#agsiobservationshape)
- [agsiGeometryFromFile](#agsigeometryfromfile)
- [agsiDataPropertyFromFile](#agsidatapropertyfromfile)
- [remarks](#remarks)


### 5.3.2. Attributes

##### observationSetID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this set of observations. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it.  If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for observationSetID should be unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``Obs/GIHolesA``

##### description
Short description of the set of observations defined here.  
*Type:* string  
*Example:* ``2018 GI Package A``

##### investigationID
For a set that represents a GI, reference to the [identifier](./Standard_General_Rules.md#1510-identifiers) for the corresponding [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object, if used.  
*Type:* string (reference to *[investigationID](./Standard_Project_agsProjectInvestigation.md#investigationid)* of [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object)  
*Example:* ``GIPackageA``

##### documentSetID
Reference to report(s) relating to this set of observations, details of which should be provided in [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)  
*Type:* string (reference to *[documentSetID](./Standard_Project_agsProjectDocumentSet.md#documentsetid)* of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``ExampleDocSetID``

##### agsiObservationExpHole
Array of embedded [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) objects.  
*Type:* array (of [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) object(s))  


##### agsiObservationPoint
Array of embedded [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) objects.  
*Type:* array (of [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) object(s))  


##### agsiObservationShape
Array of embedded [agsiObservationShape](./Standard_Observation_agsiObservationShape.md) objects.  
*Type:* array (of [agsiObservationShape](./Standard_Observation_agsiObservationShape.md) object(s))  


##### agsiGeometryFromFile
An embedded Geometry object defining an appropriate geometry for this set of observations, which will be a reference to an external file. Optional, and should only be used for geometry appropriate to the entire set. Geometry for individual observations should be defined in the embedded child observation objects.  
*Type:* object (single [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  


##### agsiDataPropertyFromFile
An embedded [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object, which may be used to reference to an external supporting data file.  
*Type:* object (single [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


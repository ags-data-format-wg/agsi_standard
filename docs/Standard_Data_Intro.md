
# AGSi Standard / 7. Data

## 7.1. Data group overview

### 7.1.1. Usage

The Data group comprises objects that can be used to assign [property](#property) and [parameter](#parameter) data to other objects, in particular
[model elements](./Standard_General_Definitions.md#model-element) and
[observations](./Standard_General_Definitions.md#observation).

The Data objects are resources that can be embedded within other schema objects, where specified in the standard (object reference).

For the purposes of the schema, a distinction between [properties](#property) and [parameters](#parameter) has been made as follows:

##### Property

A value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values.

##### Parameter

An interpreted single value, or a profile of values related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.

The Data group objects cover the following:

- single [property](#property) or [parameter](#parameter) values (numeric or text)
- profiles of (numeric) [properties](#property) against an independent variable, eg. depth or elevation
- profiles of (numeric) [parameters](#parameter), e.g. a design line
- statistical summary of properties (range, mean, standard deviation and count)
- link to a [supporting](./Standard_General_Definitions.md#agsi-supporting-files) data file


### 7.1.2. Summary of schema

This diagram shows the objects in the Data group and their relationships.

![Data summary UML diagram](./images/agsiData_summary_UML.svg)

All of the Data group objects are standalone objects that can be embedded within other schema objects, where specified in the standard (object reference).

[Property](#property) and [parameter](#parameter) values are provided using
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) and
[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)
respectively. The values provided may be numeric, text or a profile (against an independent variable), with different attributes used for each.

[agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md)
may be used, where applicable, to provide a simple statistical summary of the data.

The [properties](#property) and [parameters](#parameter) are identified by codes
(using the ***codeID*** attribute).
The meaning of the codes shall be defined in the Project group using
[agsProjectCode](./Standard_Project_agsProjectCode.md) and/or
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md).

[agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) can be used to provide a link to a
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data file. Complex data may be incorporated using this method, but users should be take account of its limitations
([see 7.2.5. Limitations of agsiDataPropertyFromFile](./Standard_Data_Rules.md#725-limitations-of-agsidatapropertyfromfile)).

Refer to [7.2. Data rules and conventions](./Standard_Data_Rules.md)
for more information on how the above should be used.


### 7.1.3. Schema UML diagram

The diagram below shows the full schema of the Data group including all attributes.
It also includes the direct parent and child objects of the Data group.
Reference links to other parts of the AGSi schema are not shown in this diagram.

<a href="../images/agsiData_full_UML.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    Guidance on how to interpret the UML diagrams shown in this standard can be found in
    <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_General_UML">Guidance > General > How to read AGSi schema diagrams</a>.


![Data full UML diagram](images/agsiData_full_UML.svg)

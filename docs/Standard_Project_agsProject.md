


# AGSi Standard / 3. Project

## 3.3. agsProject

### 3.3.1. Object description

Metadata for the specific project/commission (the [Project](./Standard_General_Definitions.md#project)) under which this [AGSi package](./Standard_General_Definitions.md#agsi-package) has been delivered. There can be only one project per [AGSi file](./Standard_General_Definitions.md#agsi-file). The parent project, including the ultimate parent project, may be identified using the relevant attributes. 

The parent object of [agsProject](./Standard_Project_agsProject.md) is [root](./Standard_Root_Intro.md)

[agsProject](./Standard_Project_agsProject.md) contains the following embedded child objects: 

- [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)
- [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md)

[agsProject](./Standard_Project_agsProject.md) has the following attributes:


- [projectUUID](#projectuuid)
- [projectName](#projectname)
- [producer](#producer)
- [producerSuppliers](#producersuppliers)
- [client](#client)
- [description](#description)
- [projectCountry](#projectcountry)
- [producerProjectID](#producerprojectid)
- [clientProjectID](#clientprojectid)
- [parentProjectName](#parentprojectname)
- [ultimateProjectName](#ultimateprojectname)
- [ultimateProjectClient](#ultimateprojectclient)
- [briefDocumentSetID](#briefdocumentsetid)
- [reportDocumentSetID](#reportdocumentsetid)
- [agsProjectCoordinateSystem](#agsprojectcoordinatesystem)
- [agsProjectInvestigation](#agsprojectinvestigation)
- [agsProjectDocumentSet](#agsprojectdocumentset)
- [agsProjectCodeSet](#agsprojectcodeset)
- [remarks](#remarks)


### 3.3.2. Attributes

##### projectUUID
Universal/global unique identifier ([UUID](./Standard_General_Rules.md#1511-uuids)) for the project. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity. Other attributes should be used for IDs specific to the producer and/or client (see below).  
*Type:* string ([UUID](./Standard_General_Rules.md#1511-uuids))  
*Example:* ``f7884d64-77ae-4eaf-b223-13c21bc2504b``

##### projectName
Name of the specific project/commission for the [Project](./Standard_General_Definitions.md#project).  
*Type:* string  
*Condition:* Required  
*Example:* ``C999 Geotechnical Package X``

##### producer
Organisation employed by the client responsible for the [Project](./Standard_General_Definitions.md#project).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``ABC Consultants``

##### producerSuppliers
If applicable, subconsultant(s) or subcontractor(s) employed  on the [Project](./Standard_General_Definitions.md#project). Typically only include suppliers with direct involvement in producing the data included in this file. Input required as a text string not an array.  
*Type:* string  
*Example:* ``Acme Environmental, AN Other Organisation``

##### client
Client for the [Project](./Standard_General_Definitions.md#project).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``XYZ D&B Contractor``

##### description
Brief project description.  
*Type:* string  
*Example:* ``Stage 3 sitewide ground modelling, including incorporation of new 2018 GI data.``

##### projectCountry
Normally the country in which the ultimate project is taking place.  
*Type:* string  
*Example:* ``United Kingdom``

##### producerProjectID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this Project used by the producer of this file. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``P12345``

##### clientProjectID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this Project used by the client. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``C999/ABC``

##### parentProjectName
If applicable, the parent project/commission under which the [Project](./Standard_General_Definitions.md#project) has been procured, or which the [Project](./Standard_General_Definitions.md#project) reports to.  
*Type:* string  
*Example:* ``C999 Area A Phase 1 Design and Build``

##### ultimateProjectName
If applicable, the top level parent project that the [Project](./Standard_General_Definitions.md#project) is ultimately for. Typically the works that are to be constructed, or a framework.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham City Metro Purple Line``

##### ultimateProjectClient
Client for the top level parent project.  
*Type:* string  
*Example:* ``City Transport Authority``

##### briefDocumentSetID
Reference to the brief and/or specification for the project, details of which should be provided by way of an [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object.  
*Type:* string (reference to *[documentSetID](./Standard_Project_agsProjectDocumentSet.md#documentsetid)* of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``ExampleDocSetID``

##### reportDocumentSetID
Reference to report(s) and other documentation produced as part of this project and identified as supporting files.   
*Type:* string (reference to *[documentSetID](./Standard_Project_agsProjectDocumentSet.md#documentsetid)* of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``ExampleDocSetID``

##### agsProjectCoordinateSystem
Array of embedded [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object(s)  
*Type:* array (of [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object(s))  


##### agsProjectInvestigation
Array of embedded [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object(s)  
*Type:* array (of [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object(s))  


##### agsProjectDocumentSet
Array of embedded [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object(s)  
*Type:* array (of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object(s))  


##### agsProjectCodeSet
Array of embedded [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object(s)  
*Type:* array (of [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object(s))  


##### remarks
Additional remarks if required.  
*Type:* string  
*Example:* ``Some remarks if required``


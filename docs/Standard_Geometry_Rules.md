# AGSi Standard / 6. Geometry

## 6.2. Geometry rules and conventions

This section details rules and conventions applicable to the Geometry group of objects. It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [1.5. General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.

###  6.2.1. File formats for geometry

The use of open standard or non-proprietary formats is recommended.

Open standard or non-proprietary formats that may be useful for geometry include:

- GeoJSON
- WKT, ISO/IEC 13249-3
- LandXML
- STEP, ISO 10303-21
- STL
- DXF (Autodesk, but interoperable)
- IFC, ISO 16739-1

For surfaces and volumes, use of formats that provide an unambiguous representation of the surface or volume is recommended. TIN (triangular irregular network) and BREP (boundary representation) are examples of this whereas grid data (XYZ points) for surfaces may allow more than one valid interpretation of the surface.

For lines, <a href="https://geojson.org/" target="_blank">GeoJSON</a> is a potentially useful format that could be used. It is possible to have a GeoJSON file that describes only a single line. However, if many lines are required, it may be inconvenient to have lots of files. An alternative approach would be to use a GeoJSON *feature collection* to house many geometries in the same file, each differentiated with their own *id*.
The [*filePart*](./Standard_Geometry_agsiGeometryFromFile.md)
attribute in [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md#filepart)
can then be used to identify which part of each file corresponds to a particular line.
However, caution should be exercised if this latter method is used as it may not be supported by some software. If GeoJSON is expected to be used, then this should be addressed in the
[specification](./Standard_General_Specification.md).

!!! Note
    The <a href="https://datatracker.ietf.org/doc/html/rfc7946" target="_blank">GeoJSON Specification (RFC 7946)</a>
    clause 4 requires *"The coordinate reference system for all GeoJSON coordinates is a geographic coordinate reference system, using the World Geodetic System 1984 (WGS 84) [WGS84] datum, with longitude and latitude units of decimal degrees."*

    However, later in the same clause it states *"However, where all involved parties have a prior arrangement, alternative coordinate reference systems can be used without risk of data being misinterpreted."*

    On this basis, use of
    [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system)
    coordinates in GeoJSON files referenced by AGSi may be considered to be such a *prior arrangement*!

The file formats to be used for the project should be described in the
[specification](./Standard_General_Specification.md).

### 6.2.2. Ambiguous or incorrect geometry

AGSi does not provide any rules or guidance for the resolution of ambiguous or incorrect geometry.

Situations that may arise include:

- Unintended overlapping or intersecting volumes or surfaces
- Missing volumetric unit due to omission of a top surface (this may not be obvious if unit bottom surfaces not also defined)
- Unintended gaps in volumetric model
- Open polygons defined (start and end coordinates not the same) where the schema and/or model require a closed polygon

Interpretation or resolution of such difficulties is the responsibility of the user(s).

### 6.2.3. Volumes from surfaces (agsiGeometryVolFromSurfaces)

#### 6.2.3.1. Recommended use

Use of explicit volumetric elements (e.g. using STEP, STL file formats) to represent solid/volumetric elements in a model is preferred.

Where this is not possible, e.g. software limitations, and
only top and/or bottom surfaces are provided to form a 3D model, it is recommended that the model element ([agsiModelElement](./Standard_Model_agsiModelElement.md) object) should use an
[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object.

Use of [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
clarifies how the surface(s) are to be put together and interpreted to form an implied volumetric model. Rules for interpretation are provided below.

If the user(s) requires different rules for interpretation of surfaces then the
[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object
shall not be used and the model elements shall directly reference
the [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
objects that define those surfaces. The project specific rules for interpretation shall be conveyed to other users via documentation, which should be referenced and included as part of the [AGSi package](./Standard_General_Definitions.md#agsi-package).

Mixing of [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) and explicit volumetric elements (e.g. using STEP, STL file formats) within a model is not recommended.

#### 6.2.3.2. Limitations

Use of surfaces that fold back on themselves (more than one Z value possible for an XY location) is not supported by this method.
Explicit volumetric elements should be used in such situations.


#### 6.2.3.3. Top and/or bottom surfaces

Definition of both top and bottom surfaces is recommended to minimise the risk of error.

!!! Note
    If only one of the top and bottom surfaces is defined, the model formed may be incorrect if a surface is inadvertently omitted. If both are defined, any anomaly arising from incomplete data should be obvious to the user viewing the model formed.

If only one of top or bottom surfaces are defined, it is the responsibility of the user(s) to ensure that the surfaces provided unambiguously describe the model.

Mixing use of 'top only' and 'bottom only' surfaces within a model is not recommended as this may give rise to ambiguity in interpretation.

Project requirements for use of the top and/or bottom surfaces should be described in the [specification](./Standard_General_Specification.md).

#### 6.2.3.4. Rules for surfaces

The following rules apply to surfaces used by [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md).
These are necessary for unambiguous interpretation.

1. Surfaces used by all [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) objects within a single model shall not cross. Surfaces may be coincident.

2. If both top and bottom surfaces are defined, the top surface of an element shall lie above or be coincident with its bottom surface.

Interpretation or resolution of non-compliance with the above rules is the responsibility of the user(s).

!!! Note
    In practice, when modelling intermittent units which require parts of some surfaces to be coincident, it may be difficult to create surfaces that are exactly coincident due to the method of modelling. This may lead to small gaps or overlaps between surfaces intended to be coincident. It is recommended that users bear this in mind. One approach would be to assume that surfaces are coincident where they lie within a specified (small) distance of each other.

#### 6.2.3.5. Interpretation

The information provided by [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) should be interpreted as follows. It is the responsibility of the user(s) to check that the relevant software/applications are using this interpretation.

!!! Note
    It is hoped that software/applications will adopt this interpretation.

Rules for interpretation:

1.  The interpretation described herein shall consider all of the surfaces defined within the collection of
[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
objects provided for a single model to construct the volumetric elements. Other surfaces that are defined by direct reference from
[agsiModelElement](./Standard_Model_agsiModelElement.md) to [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md), e.g. for water tables, shall be ignored in this interpretation.

2.  If only top surfaces are defined the volume formed for an element is between its top surface and the next top surface encountered below this (looking vertically down). The sides of the element are vertical extending down from the top surface to the next top surface. If no other top surface is encountered the volume extends down infinitely, but will be curtailed by the model bottom boundary, if used. This rule is illustrated in the diagram below.

    ![Volumes from top surfaces](./images/Geometry_Rules_VolFromSurfaces_Top.svg)

3.  If only bottom surfaces are defined the volume formed for an element is between its bottom surface and the next bottom surface encountered above this. The sides of the element are vertical extending up from the bottom surface to the next bottom surface. If no other bottom surface is encountered the volume extends up infinitely, but will be curtailed by the model top boundary, if used. When using bottom surface, it will generally be necessary to define the top boundary of the model, typically as the ground surface (terrain).

4.  If both top and bottom surfaces are defined the volume formed for an element is between its top surface and the bottom surface, which shall lie below it. The sides of the element are vertical extending down from the top surface to the bottom surface. If the bottom surface is not encountered the volume extends down infinitely, but will be curtailed by the model boundary, if used. This rule is illustrated in the diagram below.

    ![Volumes from top and bottom surfaces](./images/Geometry_Rules_VolFromSurfaces_Both.svg)

5.  The volumetric elements shall be formed using the above rules first. If a model element includes a limiting area
([*agsiGeometryAreaLimit*](./Standard_Model_agsiModelElement.md#agsigeometryarealimit
) attribute of [agsiModelElement](./Standard_Model_agsiModelElement.md))
then this limiting area is applied after the volumetric element is formed.

6. Model boundaries (using [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)) shall be applied after the volumetric elements have been formed and limiting areas applied.

!!! Note
    In practice, it is possible that the area extent of surfaces may vary from layer to layer, typically due to the method of modelling. This may lead to unintended consequences near the edge of the model.
    This may be overcome by using a model boundary
    ([agsiModelBoundary](./Standard_Model_agsiModelBoundary.md))
    to clip the errant edges of the model.

#### 6.2.3.6. Intermittent units and lenses

An intermittent unit is a volumetric unit that does not extend to the full extent of the model, i.e. it has zero thickness in some parts of the model.

A lens is an intermittent unit that is fully or partially enclosed by another unit, i.e. enclosing unit occurs both above and below the intermittent unit.

Following the above rules, two methodologies are available in relation to intermittent units when using [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md):

1.  Surfaces for an intermittent unit are extended beyond the true extent of the unit. However, coincident surfaces are defined in areas where the unit is not present, i.e. modelled unit has zero thickness.
2. Surfaces for intermittent unit are curtailed to the extent of the intermittent unit.

These are illustrated in the following diagram:

![Intermittent units](./images/Geometry_Rules_Intermittent.svg)

!!! Note
    If Method 2 is used, a separate model element will be required for each occurrence of the intermittent unit. Method 1 allows use of one model element for all occurrences.


For a lens the method of interpretation used renders it necessary to split the enclosing unit into separate model elements representing the volumes above and below the lens. A variant of method 1 may be used as shown in the diagram below:

![Lens using method 1](./images/Geometry_Rules_Lens.svg)

!!! Note
    The above solution for a lens is not ideal as it creates an artificial split in the unit. Explicit volume modelling is likely to provide a more elegant solution for lenses.

Project requirements for modelling intermittent units and lenses should be described in the [specification](./Standard_General_Specification.md).

### 6.2.4. Areas from lines (agsiGeometryAreaFromLines)

The [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) object may be used for areas defined in 2D (e.g. cross section) or 3D space.

Where the lines and areas are in 3D space, is recommended that [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) objects are only used to represent areas within vertical planes.

Recommended usage, limitations and interpretation of
[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) shall follow the principles given in
[6.2.3. Volumes from surfaces (agsiGeometryVolFromSurfaces)](#623-volumes-from-surfaces-agsigeometryvolfromsurfaces)), replacing volumes with (planar) areas and surfaces with lines.

If closed polygons are used to form areas then [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) must not be used.
The model element ([agsiModelElement](./Standard_Model_agsiModelElement.md)) should instead directly reference the polygon geometry which will be defined using an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object.

### 6.2.5. Simple 1D layers (agsiGeometryLayer)

When using [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md), use of both top and bottom elevations is recommended.

The top elevation of a layer must lie above, or be coincident with the bottom layer.

If only one of top and bottom elevations is defined, the layer is interpreted to be formed as follows:

1.  If only top surfaces are defined the layer for an element is formed between its top surface and the next top surface encountered below this. If no other top surface is encountered the layer extends down infinitely, but will be curtailed by the model bottom boundary, if used.

2.  If only bottom surfaces are defined the layer for an element is formed between its bottom surface and the next bottom surface encountered above this. If no other bottom surface is encountered the  layer extends up infinitely, but will be curtailed by the model top boundary, if used.

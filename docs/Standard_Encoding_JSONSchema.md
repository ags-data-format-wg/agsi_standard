# AGSi Standard / 8. Encoding

## 8.3. JSON Schema

<a href="https://json-schema.org/" target="_blank">JSON Schema</a>
provides a method for describing a schema that may be used for validation. Validation tools using JSON Schema are available online.

JSON Schema for this version of AGSi are provided in support this standard. They can be downloaded from <a href="../schema/AGSi_JSONSchema_v1-0-1.zip" target="_blank">../schema/AGSi_JSONSchema_v1-0-1.zip</a>

Two alternate version of the JSON Schema are provided corresponding to versions 2019-09 and 2020-12 of JSON Schema (see 
<a href="https://json-schema.org/specification" target="_blank">JSON Schema specifications</a>).

!!! Note

    At time of writing, the most recent Draft 2020-12 was not well supported by validation tools. Therefore we have also included a version 2019-09 schema, which is better supported.

    However, for AGSi the JSON Schema file is identical for both of these versions, bar the reference to the version of the schema at the top of the file! Thus you should get identical results regardless of which version you use in any compatible validator.

In the event of any conflict between the JSON Schema file provided and this standard, the standard shall take precedence.

Validation against the JSON schema will normally include the following:

* JSON syntax
* Object names and their relationships
* Attribute names
* Attribute data types
* Required objects or attributes
* Additional format rules for strings, e.g. date/time format
* Specific conditional requirements of the schema, e.g. one or other of two fields must be populated

!!! Note

    In the JSON Schema specification format checking is not mandatory. As a result, format checking may not be incorporated in all validation applications,
    or it may be an optional extra. AGSi requires format checking, therefore format checking should be activated if not already the case.

There are some aspects that are critical to the correct formation of an AGSi file that
will **not** generally be checked by general purpose JSON Schema validation software.
These include:

* Cross-check on identifiers used for reference links between objects
* Check for uniqueness of Identifiers
* Check that supporting files are accessible

The AGSi validation tool being developed by AGS will include these additional checks.

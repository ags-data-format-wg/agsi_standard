# AGSi Standard / 4. Model

## 4.1. Model group overview

### 4.1.1. Usage

In an AGSi context, a <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_Model_General#model">model</a>
is a digital geometric (1D, 2D or 3D) representation of the ground.

The Model group is used to define and assemble geometry based models. Each model is defined using an [agsiModel](./Standard_Model_agsiModel.md) object.

These models are built up from elements which have their geometry defined by embedded geometry objects from the [Geometry group](./Standard_Geometry_Intro.md).
Model elements may also include data by embedding objects from the [Data group](./Standard_Data_Intro.md).

A model may also contain embedded sets of observations from the [Observation group](./Standard_Observation_Intro.md).

There may be more than one model defined in an
[AGSi file](./Standard_General_Definitions.md#agsi-file).

For further information on what is considered to be a single model in an AGSi context,
please refer to <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_Model_General">Guidance on usage, Model - Overview</a>.


### 4.1.2. Summary of schema

This diagram shows the objects in the Model group and their relationships.

![Model summary UML diagram](./images/agsiModel_summary_UML.svg)

A single model is defined by an [agsiModel](./Standard_Model_agsiModel.md) object.
There may be several [agsiModel](./Standard_Model_agsiModel.md) objects in an
[AGSi file](./Standard_General_Definitions.md#agsi-file).
The [agsiModel](./Standard_Model_agsiModel.md) objects are embedded, within an array, in the [root object](./Standard_Root_Intro.md).
Each [agsiModel](./Standard_Model_agsiModel.md) object contains objects from the classes described below.

The basic building blocks of each model are model elements.
The model elements are defined as [agsiModelElement](./Standard_Model_agsiModelElement.md) objects.
These elements are embedded (within an array) in the parent [agsiModel](./Standard_Model_agsiModel.md) object.

Geometry is assigned to model elements by embedding any single object from the
[Geometry group](./Standard_Geometry_Intro.md) using the
*[agsiGeometry](./Standard_Model_agsiModelElement.md#agsigeometry)* attribute.
In many cases this will be an
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object referencing a
[supporting file](./Standard_General_Definitions.md#agsi-supporting-files)
containing the geometry data.

Data ([properties](./Standard_General_Definitions.md#property) or
[parameters](./Standard_General_Definitions.md#parameter)) may be assigned to model elements by embedding
[Data group objects](./Standard_Data_Intro.md) using the applicable attributes.

In addition, a model boundary may also be specified using an [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
object. Each model may contain only one [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md), which will be embedded within the parent [agsiModel](./Standard_Model_agsiModel.md).

Alignments, which are lines of interest in the model (typically used for section lines) may be defined using [agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) objects.
[agsiModelAlignment](./Standard_Model_agsiModelAlignment.md) objects are embedded within the parent [agsiModel](./Standard_Model_agsiModel.md). However, it is also possible to reference an alignment in a different model by using the
[*alignmentID*](./Standard_Model_agsiModel.md#alignmentid) attribute in
[agsiModel](./Standard_Model_agsiModel.md)

Models may also contain observations from the [Observation group](./Standard_Observation_Intro.md). This is achieved by embedding [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) objects.
The observations may in turn contain embedded data from the [Data group](./Standard_Data_Intro.md) and some may include geometry from the [Geometry group](./Standard_Geometry_Intro.md). [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) objects are embedded within the parent [agsiModel](./Standard_Model_agsiModel.md).


### 4.1.3. Schema UML diagram

The diagram below shows the full schema of the Model group including all attributes.
It also includes the direct parent and child objects of the Model group.
Reference links to other parts of the AGSi schema are not shown in this diagram.

<a href="../images/agsiModel_full_UML.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    Guidance on how to interpret the UML diagrams shown in this standard can be found in
    <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_General_UML">Guidance > General > How to read AGSi schema diagrams</a>.


![Model full UML diagram](./images/agsiModel_full_UML.svg)

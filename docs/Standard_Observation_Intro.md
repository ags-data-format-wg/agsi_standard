
# AGSi Standard / 5. Observation

## 5.1. Observation group overview

### 5.1.1. Usage

The Observation group comprises objects that can be used to describe
[observations](./Standard_General_Definitions.md#observation),
or sets thereof. These observations are included as part of an [agsiModel](./Standard_Model_agsiModel.md) object.

In an AGSi context, observations comprise spatially referenced data or information provided as part of the model, or in support of it. Observations include, but are not necessarily limited to, data and information used to derive the model.

In AGSi, observations may include:

- factual data from ground investigations
- field observations
- data from surveys
- findings from desk study and other studies

AGSi observations are most likely to comprise
[factual data](./Standard_General_Definitions.md#factual-data),
but there may be situations where it is appropriate to include observations that are based on [interpretation](./Standard_General_Definitions.md#interpreted-data).

The primary focus of AGSi is ground models and interpreted data. AGSi is not intended to replace or replicate the <a href="https://www.ags.org.uk/data-format/" target="_blank">existing AGS format</a>
for transfer of factual data from ground investigations (GI), or other established formats for transfer of factual data.
Observations in AGSi should only be used to convey significant information that adds value to the model, taking into account its intended usage.

For more detailed information on usage of observations in AGSi, refer to [5.2. Observation rules and conventions](./Standard_Observation_Rules.md).

Users, in particular specifiers where applicable, should decide what data and information should be included as observations.

### 5.1.2. Summary of schema

This diagram shows the objects in the Observation group and their relationships.

![Observation summary UML diagram](./images/agsiObservation_summary_UML.svg)

The Observation group provides resources that can be used within an AGSi model.
Specifically, [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) objects are embedded (as an array of child objects) in [agsiModel](./Standard_Model_agsiModel.md).

There are two types of observation objects which cover, respectively:

- ground investigation (GI) exploratory holes and their data ([agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) and [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md))
- other types of observation ([agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) and [agsiObservationShape](./Standard_Observation_agsiObservationShape.md))

[agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md) may be used to identify and locate individual GI exploratory holes. It includes basic metadata for the holes. Further data, which could be detailed hole metadata, hole formation data or test data from the hole, can be provided by using embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. A
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data file may be referenced using [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md).

The geological or lithological succession within an exploratory hole can be provided using [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md), which is a child object of [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md). [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) may also be used for other data associated with a depth/elevation range (from and to) by embedding [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects.

Observations of any type that relate to a specific (spatial) point can be provided using [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md), which includes attributes for basic metadata and a single text based description/value for the observation/measurement. More complex structured data can be provided using embedded [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects. A
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data file may be referenced using [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md).

Observations that relate to a shape rather than a point can be provided using [agsiObservationShape](./Standard_Observation_agsiObservationShape.md). The shape, which may be a line, area, volume or series of points, is defined via an embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Data is provided as described above for [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md).

All observations shall be contained with a parent [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) object, which may also be used to provide metadata or data for the 'set' as a whole. An [agsiObservationSet](./Standard_Observation_agsiObservationSet.md) object may contain different types of observation: [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md), [agsiObservationPoint](./Standard_Observation_agsiObservationPoint.md) or [agsiObservationShape](./Standard_Observation_agsiObservationShape.md) ([agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md) objects are embedded via their parent [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md)).

For more detailed information on how the schema should be used for observations, refer to [5.2. Observations rules and conventions](./Standard_Observation_Rules.md).


### 5.1.3. Schema UML diagram

The diagram below shows the full schema of the Observation group including all attributes.
It also includes the direct parent and child objects of the Observation group.
Reference links to other parts of the AGSi schema are not shown in this diagram.

<a href="../images/agsiObservation_full_UML.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    Guidance on how to interpret the UML diagrams shown in this standard can be found in
    <a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_General_UML">Guidance > General > How to read AGSi schema diagrams</a>.


![Observation full UML diagram](./images/agsiObservation_full_UML.svg)

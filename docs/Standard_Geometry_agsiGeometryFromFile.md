


# AGSi Standard / 6. Geometry

## 6.3. agsiGeometryFromFile

### 6.3.1. Object description

An [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object is a pointer to geometry data contained within an external file, such as a CAD or model file. This object also includes metadata describing the file being referenced. Refer to [6.2. Geometry rules and conventions](./Standard_Geometry_Rules.md) for further requirements and recommendations relating to this object.

[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) may be embedded in any of the following potential parent objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
-  [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
-  [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
-  [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
-  [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)
-  [agsiObservationShape](./Standard_Observation_agsiObservationShape.md)

[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [geometryType](#geometrytype)
- [fileFormat](#fileformat)
- [fileFormatVersion](#fileformatversion)
- [fileURI](#fileuri)
- [filePart](#filepart)
- [revision](#revision)
- [date](#date)
- [revisionInfo](#revisioninfo)
- [remarks](#remarks)


### 6.3.2. Attributes

##### geometryID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this geometry object. May be local to this file but all [identifiers](./Standard_General_Rules.md#1510-identifiers) used within the Geometry group of objects shall be unique. Alternatively a [UUID](./Standard_General_Rules.md#1511-uuids) may be used (recommended for large datasets). Use of this attribute is optional and it is not referenced anywhere else in the schema, but it may be beneficial to include it to help with data control and integrity, and some applications may require or benefit from it.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``GeologyGCCTop``

##### description
Short description of geometry defined here.  
*Type:* string  
*Example:* ``Top of GC``

##### geometryType
Nature of geometry represented.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md#922-used-in-geometry-group))  
*Condition:* Recommended  
*Example:* ``Surface``

##### fileFormat
Format/encoding of the data, i.e. file format. Refer to vocabulary for list of common formats that may be used, or provide concise identification if other format used. Refer to [6.2.1. File formats for geometry](./Standard_Geometry_Rules.md#621-file-formats-for-geometry) for requirements and recommendations relating to file formats.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md#922-used-in-geometry-group))  
*Condition:* Recommended  
*Example:* ``LandXML``

##### fileFormatVersion
Additional version information for file format used, if required.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``2.0``

##### fileURI
[URI-reference](./Standard_General_Definitions.md#uri-reference) for the geometry file. This will be a relative link if file is included as part of the [AGSi package](./Standard_General_Definitions.md#agsi-package). Alternatively, a link to a project document system location. Refer to [6.2.1. File formats for geometry](./Standard_Geometry_Rules.md#621-file-formats-for-geometry) for requirements and recommendations relating to linked files. Spaces are not permitted in [URI-reference](./Standard_General_Definitions.md#uri-reference) strings. Refer to [ 1.6.6. URI](./Standard_General_Formats.md#166-uri) for how to handle spaces in file paths or names.  
*Type:* string ([uri-reference](./Standard_General_Formats.md#167-uri-reference))  
*Condition:* Required  
*Example:* ``geometry/geology/GCtop.xml``

##### filePart
Pointer to a specific part of a file, where required for disambiguation. For CAD or model files this could be used for the layer/level on which the required data is located. For a geoJSON file with a feature collection this could be used to specify the id of the feature of interest.  Use with caution as the ability to interrogate only a specified layer/level/feature etc. may not be supported in all software.  
*Type:* string  
*Example:* ``GCTop``

##### revision
Revision of the referenced file.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``P2``

##### date
Date of issue of this revision.  
*Type:* string ([ISO date](./Standard_General_Formats.md#165-iso-date-andor-time))  
*Example:* ``2018-10-07``

##### revisionInfo
Revision notes for this revision of the referenced file.  
*Type:* string  
*Example:* ``Updated for GIR rev P2. Additional BH from 2018 GI included.``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``


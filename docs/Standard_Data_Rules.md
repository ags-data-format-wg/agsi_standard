
# AGSi Standard / 7. Data

## 7.2. Data rules and conventions

This section details rules and conventions applicable to the Data group of objects.
It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [1.5. General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.

### 7.2.1. Properties and parameters

For the purposes of the schema, a distinction between properties and
parameters has been made as follows:

##### Property

A value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values.

##### Parameter

An interpreted single value, or a profile of values related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.

### 7.2.2. Incorporation of factual data in AGSi

AGSi should not incorporate the full factual data used to derive the model.

Factual data should be provided in AGS (factual) format file(s) which should be identified as documents using [agsProjectDocument](./Standard_Project_agsProjectDocument.md). Reference to these documents can be made from several places in the schema, including the [*documentSetID*](./Standard_Observation_agsiObservationSet.md#documentsetid) attribute
of [agsiObservationSet](./Standard_Observation_agsiObservationSet.md).

If required, AGSi may include selected factual data using
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
objects embedded within
[Observation group ](./Standard_Observation_Intro.md) objects.

### 7.2.3. Codes

Use of the AGSi list of standard codes ([9.1. Code list](./Codes_Codelist.md)) is recommended. If this is the case, use of [agsProjectCode](./Standard_Project_agsProjectCode.md) objects to define the codes is optional, provided that use of the standard list is confirmed in
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md).

If the AGSi list of standard codes ([9.1. Code list](./Codes_Codelist.md))
is used but additional non-standard codes are required for items not covered, then the additional codes shall be defined using [agsProjectCode](./Standard_Project_agsProjectCode.md).

If an alternative code list is used, then the codes shall be defined using [agsProjectCode](./Standard_Project_agsProjectCode.md) objects.

Project requirements for codes should be described in the
[specification](./Standard_General_Specification.md).

### 7.2.4. Use of (data) case

Use of the *caseID* attribute of
[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md),
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
and
[agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md)
is optional. Either a code or text may be input.
If a code is used, this should be defined using
[agsProjectCode](./Standard_Project_agsProjectCode.md).

Examples of how *caseID* may be used are given in the
<a href="https://ags-data-format-wg.gitlab.io/agsi/AGSi_Documentation/Guidance_Data_General#use-of-case">Guidance (Data-General)</a>.

Project requirements for use of case should be described in the
[specification](./Standard_General_Specification.md).

### 7.2.5. Limitations of agsiDataPropertyFromFile

The [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) object can be used to provide a link to a
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data files.

Complex data may be incorporated using this method. However, links to
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data only provide the location of that data, with a broad description and, if used, some version metadata. Generally, it will not be possible for users receiving the data to be able to parse or otherwise use it, unless it conforms to an agreed specification.

Therefore, this object should not be routinely used to provide data that end users are likely to want to import to an application or other process.
Use of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) is preferred in such cases.

In theory, AGS (factual) data may be provided in this way by linking it to a suitable high level object. However, this is not neccesary as AGS data can also be linked using
[agsProjectDocument](./Standard_Project_agsProjectDocument.md) and
[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md).

One possible use of [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)
would be to provide a full rich data set to accompany summary/key results that have been provided using
[agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md) or[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md).
